package main.view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import main.model.RobotModel;

/**
 * Les instances de la classe RobotView constitueront les robots du jeu.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class RobotView extends RobotModel {   
	
	private ImageIcon octopunk;
	private JLabel robotContainer;
	
	/**
	 * Initialise une nouvelle instance de la classe RobotView.
	 */
	public RobotView() {
		super("Octopunk");
		robotContainer = new JLabel();
		setRobotStyle();
		octopunk = new ImageIcon (getClass().getResource("Octopunk Robot.png"));
		
		//Redimensionnement de l'image
		Image img = octopunk.getImage();  
		Image newimg = img.getScaledInstance((int) getRobotContainer().getPreferredSize().getWidth() - 90, (int) getRobotContainer().getPreferredSize().getWidth() - 90,  Image.SCALE_SMOOTH) ;  
		octopunk = new ImageIcon(newimg);
		
		robotContainer.setIcon(octopunk);
	}
	
	/**
	 * @return	l'image du robot
	 * @pure
	 */
	public ImageIcon getOctopunk() {
		return octopunk;
	}
	
	/**
	 * @return	le contenant du robot
	 * @pure
	 */
	public JLabel getRobotContainer() {
		return robotContainer;
	}
	
	/**
	 * Modifie l'image du robot.
	 * 
	 * @param newOctopunk	la nouvelle image du robot
	 */
	public void setOctopunk (ImageIcon newOctopunk) {
		//Redimensionnement de l'image
		Image img = newOctopunk.getImage();  
		Image newimg = img.getScaledInstance((int) getRobotContainer().getPreferredSize().getWidth() - 90, (int) getRobotContainer().getPreferredSize().getHeight() - 90,  Image.SCALE_SMOOTH) ;  
		this.octopunk = new ImageIcon(newimg);
	
		getRobotContainer().setIcon(octopunk);
	}
	
	/**
	 * Ensemble des modifications graphique du robot.
	 */
	public void setRobotStyle() {  
		//Modification de la dimension et du layout du JLabel contenant le robot
		robotContainer.setPreferredSize(new Dimension(150, 100));
		robotContainer.setLayout(new GridLayout(1, 1));
		robotContainer.setOpaque(false);
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof RobotView)) {
			return false;
		}
		RobotView b = (RobotView) o;
		if (octopunk != b.getOctopunk()) {
			return false;
		}
		if (robotContainer != b.getRobotContainer()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override
	public int hashCode() {
		return super.hashCode() * 103;
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Robot moving around the game zone.\n";
	}
}
