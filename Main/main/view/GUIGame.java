package main.view;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * Les instances de la classe GUIGame constitueront la fenêtre du jeu.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class GUIGame extends JFrame implements WindowListener {

	private static final long serialVersionUID = -8903203546792274040L;
	
	private JLabel background;
	private ImageIcon backgroundImage;
	private ButtonView step;
	private ButtonView stop;
	private ButtonView auto;
	private ControlZone console;
	private MemoryZone memoryZone_X;
	private MemoryZone memoryZone_T;	
	private GameZoneView gameZone;
	private boolean buttonInAction;
	private Clip clip;
	private AudioInputStream audio;
	
	/**
	 * Initialise une nouvelle instance de la classe GUIGame.
	 */
	public GUIGame() {
		super("Octopunk");
		initGUI();
	}
	
	/**
	 * Le clip va permettre de modifier la sortie audio en la musique du jeu.
	 * 
	 * @return	le clip du jeu 
	 * @pure
	 */
	public Clip getClip() {
		return clip;
	}
	
	/**
	 * L'entrée audio récupère la musique du jeu.
	 * 
	 * @return	l'entrée audio
	 * @pure
	 */
	public AudioInputStream getAudio() {
		return audio;
	}

	/**
	 * @return	le booléen indiquant si un des bouton est en action
	 * @pure
	 */
	public boolean isButtonInAction() {
		return buttonInAction;
	}
	
	/**
	 * @return	le contenant de l'image de fond
	 * @pure
	 */
	public JLabel getBackgroundGUI() {
		return background;
	}
	
	/**
	 * @return	l'image de fond
	 * @pure
	 */
	public ImageIcon getBackgroundImage() {
		return backgroundImage;
	}
	
	/**
	 * @return	le bouton step
	 * @pure
	 */
	public ButtonView getStepButton() {
		return step;
	}
	
	/**
	 * @return	le bouton stop
	 * @pure
	 */
	public ButtonView getStopButton() {
		return stop;
	} 
	
	/**
	 * @return	le bouton auto
	 * @pure
	 */
	public ButtonView getAutoButton() {
		return auto;
	}
	
	/**
	 * @return	la zone dans laquelle l'utilisateur entre son code
	 * @pure
	 */
	public ControlZone getConsole() {
		return console;
	}
	
	/**
	 * @return	la zone mémoire X du robot
	 * @pure
	 */
	public MemoryZone getXMemoryZone() {
		return memoryZone_X;
	}
	
	/**
	 * @return	la zone mémoire T du robot
	 * @pure
	 */
	public MemoryZone getTMemoryZone() {
		return memoryZone_T;
	}
	
	/**
	 * @return	la zone de jeu
	 * @pure
	 */
	public GameZoneView getGameZone() {
		return gameZone;
	}
	
	/**
	 * Met à jour le booléen indiquant si un des bouton est en action.
	 * 
	 * @param somethingIsInAction le nouveau booléen indiquant si un des bouton est en action
	 */
	public void setButtonInAction(boolean somethingIsInAction) {
		this.buttonInAction = somethingIsInAction;
	}
	
	/**
	 * Ensemble des modifications graphique de la fenêtre du jeu.
	 */
	public void initGUI() {
		buttonInAction = false;
		//Changement de la taille de la fenêtre à l'ouverture (ouverture en plein écran)
		this.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		//Ferme fenêtre quand on clique sur sa croix 
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		//Centre la fenêtre
		this.setLocationRelativeTo(null);
		//Change le positionnement des composant
		this.setLayout(null);
		//Empêche l'utilisateur de changer la taille de la fenêtre
		this.setResizable(false);
		//Ajout du Listener
		this.addWindowListener(this);
		
		//Mise en place de la musique de fond
		try {
			audio = AudioSystem.getAudioInputStream(getClass().getResource("Background sound.wav"));
		}
		catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			clip = AudioSystem.getClip();
		} 
		catch (LineUnavailableException e) {
			e.printStackTrace();
		}
		
		//Mise en place de l'image d'arrière plan
		backgroundImage = new ImageIcon(getClass().getResource("Background GUI.jpg"));
		background = new JLabel(backgroundImage);
		background.setSize(getSize());
		this.getContentPane().add(background);
		//Modifie la couleur de fond au cas où l'image de fond n'est pas assez grande pour l'écran 
		this.getContentPane().setBackground(Color.DARK_GRAY);
		
		//Création des instances composant la fenêtre  
		//Création des différents boutons
		step = new ButtonView("STEP");
		stop = new ButtonView("STOP");
		auto = new ButtonView("AUTO");
		//Création de la zone de code
		console = new ControlZone();
		//Création de la zone de Jeu
		gameZone = new GameZoneView(console);
		//Création des zones mémoires
		memoryZone_X = new MemoryZone("X");
		memoryZone_T = new MemoryZone("T");
		
		//Ajout des instances dans la fenêtre
		background.add(step);
		background.add(stop);
		background.add(auto);
		background.add(console);
		background.add(gameZone);
		background.add(memoryZone_X.getMemoryZone());
		background.add(memoryZone_T.getMemoryZone());
		
		//Positionnement des éléments dans la fenêtre
		console.setBounds(100, 275, (int) console.getPreferredSize().getWidth(), (int) console.getPreferredSize().getHeight());
		memoryZone_X.getMemoryZone().setBounds(console.getX(), console.getY() + 30 + (int) console.getPreferredSize().getHeight(), (int) memoryZone_X.getMemoryZone().getPreferredSize().getWidth(), (int) memoryZone_T.getMemoryZone().getPreferredSize().getHeight());
		memoryZone_T.getMemoryZone().setBounds(memoryZone_X.getMemoryZone().getX() + (int) memoryZone_X.getMemoryZone().getPreferredSize().getWidth() + 50, console.getY() + 30 + (int) console.getPreferredSize().getHeight(), (int) memoryZone_T.getMemoryZone().getPreferredSize().getWidth(), (int) memoryZone_T.getMemoryZone().getPreferredSize().getHeight());
		step.setBounds(console.getX(), memoryZone_T.getMemoryZone().getY() + 30 + (int) memoryZone_T.getMemoryZone().getPreferredSize().getHeight(), (int) step.getPreferredSize().getWidth(), (int) step.getPreferredSize().getHeight());
		stop.setBounds(step.getX() + (int) step.getPreferredSize().getWidth() + 50, memoryZone_T.getMemoryZone().getY() + 30 + (int) memoryZone_T.getMemoryZone().getPreferredSize().getHeight(), (int) stop.getPreferredSize().getWidth(), (int) stop.getPreferredSize().getHeight());
		auto.setBounds(console.getX(), step.getY() + 30 + (int) step.getPreferredSize().getHeight(), (int) console.getPreferredSize().getWidth(), (int) auto.getPreferredSize().getHeight());
		gameZone.setBounds((int) this.getSize().getWidth() - 100 - (int) gameZone.getPreferredSize().getWidth(), (int) this.getSize().getHeight() / 2 - (int) gameZone.getPreferredSize().getHeight() / 2 - 50, (int) gameZone.getPreferredSize().getWidth(), (int) gameZone.getPreferredSize().getHeight());
		
		//Modification de l'icon du jeu
		ImageIcon icon = new ImageIcon(getClass().getResource("Octopunk Robot.png"));
		this.setIconImage(icon.getImage());
	}
	
	/**
	 * Redéfinition des méthodes de WindowListener.
	 */
	@Override
	public void windowOpened(WindowEvent e) {
		try {
			clip.open(audio);
		} 
		catch (LineUnavailableException r) {
			r.printStackTrace();
		} 
		catch (IOException i) {
			i.printStackTrace();
		}
		clip.loop(Clip.LOOP_CONTINUOUSLY);
	}

	@Override
	public void windowClosing(WindowEvent e) {
		return;
	}

	@Override
	public void windowClosed(WindowEvent e) {
		clip.stop();
	}

	@Override
	public void windowIconified(WindowEvent e) {
		return;
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		return;
	}

	@Override
	public void windowActivated(WindowEvent e) {
		return;
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		return;
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof GUIGame)) {
			return false;
		}
		GUIGame b = (GUIGame) o;
		if (background != b.getBackgroundGUI()) {
			return false;
		}
		if (backgroundImage != b.getBackgroundImage()) {
			return false;
		}
		if (!step.equals(b.getStepButton())) {
			return false;
		}
		if (!stop.equals(b.getStopButton())) {
			return false;
		}
		if (!auto.equals(b.getAutoButton())) {
			return false;
		}
		if (!console.equals(b.getConsole())) {
			return false;
		}
		if (!memoryZone_X.equals(b.getXMemoryZone())) {
			return false;
		}
		if (!memoryZone_T.equals(b.getTMemoryZone())) {
			return false;
		}
		if (!gameZone.equals(b.getGameZone())) {
			return false;
		}
		if (clip != b.getClip()) {
			return false;
		}
		if (audio != b.getAudio()) {
			return false;
		}
		if (buttonInAction != b.isButtonInAction()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override
	public int hashCode() {
		return step.hashCode() * 2 + stop.hashCode() * 3 + auto.hashCode() * 37 + console.hashCode() * 47 + memoryZone_X.hashCode() * 67 + memoryZone_T.hashCode() * 97 + gameZone.hashCode() * 7 + (buttonInAction == true ? 2 : 19);
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Octopunks game window made up of different buttons, a game zone and a text zone.\n";
	}

}
