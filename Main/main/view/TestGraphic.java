package main.view;

import java.awt.Toolkit;

import javax.swing.JFrame;

/**
 * La classe TestGraphic permettait de tester visuellement les instances issues des classes View avant l'implémentation du main. Elle est obsolète.
 */
public class TestGraphic extends JFrame {
	
	private static final long serialVersionUID = 307033433581879367L;
	//NE PAS OUBLIER DE CHANGER L'ICON
	public TestGraphic () {
//		super("Octopunks");
//		
//		//Changement de la taille de la fenêtre à l'ouverture
//		this.setSize(Toolkit.getDefaultToolkit().getScreenSize());
//		//ferme quand on clique sur la croix de la fenêtre
//		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
////		//rend la fenêtre visible
////		this.setVisible(true);
//		//Centre la fenêtre
//		this.setLocationRelativeTo(null);
//		//Change le positionnement des composant A CHANGER
//		this.setLayout(null);
////		this.setLayout(new FlowLayout());
//		//Empêche le redimensionnement de la fenêtre
//		this.setResizable(false);
		
		
//		//Récupération du contentPane pour les modifications
//		Container contentPane = this.getContentPane();
//		
//		ImageIcon icone = new ImageIcon(getClass().getResource("Background GUI.jpg"));
////		Image img = icone.getImage();  
////		System.out.println("this : w = "+this.getPreferredSize().getWidth());
////		Image newimg = img.getScaledInstance((int) this.getPreferredSize().getWidth(), (int) this.getPreferredSize().getHeight(),  Image.SCALE_SMOOTH) ;  
////		icone = new ImageIcon(newimg);
//		JLabel background = new JLabel(icone);
//		background.setSize(getSize());
////		background.setBounds(0, 0, (int) this.getPreferredSize().getWidth(), (int) this.getPreferredSize().getHeight());
//		contentPane.add(background);
//		
//		//Changement de la couleur du fond de la fenêtre A CHANGER
////		Color couleurFond = new Color(50, 100, 100);
////		contentPane.setBackground(couleurFond);
//		
//		//Création des instances composant la fenêtre   A COMPLETER
//		//Création des différents boutons
//		ButtonView pas = new ButtonView("STEP");
//		ButtonView stop = new ButtonView("STOP");
//		ButtonView auto = new ButtonView("AUTO");
//		//Création de la zone de code
//		ControlZone console = new ControlZone();
//		//Création de la zone de Jeu
//		GameZoneView zoneDeJeu = new GameZoneView();
//		//Création des zones mémoires
//		MemoryZone zoneMemoire_X = new MemoryZone("X");
//		MemoryZone zoneMemoire_F = new MemoryZone("T");
//		
//		
//		//Ajout des instances dans le contentPane/le corps de la fenêtre 
//		background.add(pas);
//		background.add(stop);
//		background.add(auto);
//		background.add(console);
//		background.add(zoneDeJeu);
//		background.add(zoneMemoire_X.getMemoryZone());
//		background.add(zoneMemoire_F.getMemoryZone());
//		
//		console.setBounds(100, 275, (int) console.getPreferredSize().getWidth(), (int) console.getPreferredSize().getHeight());
//		zoneMemoire_X.getMemoryZone().setBounds(100, console.getY() + 30 + (int) console.getPreferredSize().getHeight(), (int) zoneMemoire_X.getMemoryZone().getPreferredSize().getWidth(), (int) zoneMemoire_F.getMemoryZone().getPreferredSize().getHeight());
//		zoneMemoire_F.getMemoryZone().setBounds(zoneMemoire_X.getMemoryZone().getX() + (int) zoneMemoire_X.getMemoryZone().getPreferredSize().getWidth() + 50, console.getY() + 30 + (int) console.getPreferredSize().getHeight(), (int) zoneMemoire_F.getMemoryZone().getPreferredSize().getWidth(), (int) zoneMemoire_F.getMemoryZone().getPreferredSize().getHeight());
//		pas.setBounds(100, zoneMemoire_F.getMemoryZone().getY() + 30 + (int) zoneMemoire_F.getMemoryZone().getPreferredSize().getHeight(), (int) pas.getPreferredSize().getWidth(), (int) pas.getPreferredSize().getHeight());
//		stop.setBounds(pas.getX() + (int) pas.getPreferredSize().getWidth() + 50, zoneMemoire_F.getMemoryZone().getY() + 30 + (int) zoneMemoire_F.getMemoryZone().getPreferredSize().getHeight(), (int) stop.getPreferredSize().getWidth(), (int) stop.getPreferredSize().getHeight());
//		auto.setBounds(100, pas.getY() + 30 + (int) pas.getPreferredSize().getHeight(), (int) console.getPreferredSize().getWidth(), (int) auto.getPreferredSize().getHeight());
//		zoneDeJeu.setBounds((int) this.getSize().getWidth() - 100 - (int) zoneDeJeu.getPreferredSize().getWidth(), (int) this.getSize().getHeight() / 2 - (int) zoneDeJeu.getPreferredSize().getHeight() / 2 - 50, (int) zoneDeJeu.getPreferredSize().getWidth(), (int) zoneDeJeu.getPreferredSize().getHeight());
//
////		100 + (int) console.getPreferredSize().getWidth() +  100
//		
////		Dimension dimensionStop = new Dimension(200, 300);
////		stop.setPreferredSize(dimensionStop);
////		System.out.println("stop : Width = "+stop.getWidth());
////		System.out.println("dimensionStop : Width = " + dimensionStop.getWidth());
//		
		
//		Dialog test1 = new Dialog(this, "test");
//		//rend la fenêtre visible
//		this.setVisible(true);
		
		
		//Modification de l'icon du jeu
//		ImageIcon icon = new ImageIcon(getClass().getResource("Octopunk Robot.png"));
		//Redimensionnement de l'image
//		Image img = icon.getImage();  
//		Image newimg = img.getScaledInstance(50, 50,  Image.SCALE_SMOOTH) ;  
//		icon = new ImageIcon(newimg);
//		this.setIconImage(icon.getImage());
		
	}
	
	public static void main(String[] args) {
		
//		GUIGame game = new GUIGame();
//		game.setVisible(true);
		
		TestGraphic test = new TestGraphic();
		
	}
	
	
}
