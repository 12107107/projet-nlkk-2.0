package main.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import main.model.Register;

/**
 * Les instances de la classe MemoryZone constitueront les zones mémoires du robot.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class MemoryZone extends Register {
	
	private JPanel memoryZone;
	private JLabel title;
	private JLabel content;
	private Color titleColor;
	public Color contentColor;
	
	/**
	 * Initialise une nouvelle instance de la classe MemoryZone.
	 * 
	 * @param title le nom de la zone mémoire
	 */
	public MemoryZone (String title) {
		super(title);
		memoryZone = new JPanel();
		this.title = new JLabel(title);
		content = new JLabel();
		contentColor = new Color(87, 184, 188);
		titleColor = new Color(37,159,175);
		setMemoryZoneStyle();
	}
	
	/**
	 * @return	le contenant de la zone mémoire
	 * @pure
	 */
	public JPanel getMemoryZone() {
		return memoryZone;
	}
	
	/**
	 * @return	le contenant du nom de la zone mémoire
	 * @pure
	 */
	public JLabel getTitle() {
		return title;
	}
	
	/**
	 * @return	le contenant du résultat des opérations entrées par l'utilisateur de la zone mémoire
	 * @pure
	 */
	public JLabel getContent() {
		return content;
	}
	
	/**
	 * @return	la couleur de fond du nom de la zone mémoire
	 * @pure
	 */
	public Color getTitleColor() {
		return titleColor;
	}
	
	/**
	 * @return	la couleur de fond du résultat des opérations entrées par l'utilisateur de la zone mémoire
	 * @pure
	 */
	public Color getContentColor() {
		return contentColor;
	}
	
	/**
	 * Modifie le contenu de la zone mémoire.
	 * 
	 * @param newContent le résultat de l'opération qui vient d'être effectuée (le nouveau contenu de la zone mémoire)
	 */
	public void setContent (String newContent) { 
		getContent().setText(newContent);
	}
	
	/**
	 * Modifie le titre de la zone mémoire.
	 * 
	 * @param newTitle le nouveau nom de la zone mémoire
	 */
	public void setTitle (String newTitle) {
		getTitle().setText(newTitle);
	}
	
	/**
	 * Modifie la couleur de fond du contenu de la zone mémoire.
	 * 
	 * @param newContentColor la nouvelle couleur de fond du contenu de la zone mémoire
	 */
	public void setColorContent(Color newContentColor) {
		getContent().setBackground(newContentColor);
		contentColor = newContentColor;
	}
	
	/**
	 * Modifie la couleur de fond du nom de la zone mémoire.
	 * 
	 * @param newTitleColor	la nouvelle couleur de fond du nom de la zone mémoire
	 */
	public void setTitleColor (Color newTitleColor) {
		getTitle().setBackground(newTitleColor);
		titleColor = newTitleColor;
	}
	
	/**
	 * Ensemble des modifications graphique de la zone mémoire du robot.
	 */
	public void setMemoryZoneStyle() {
		//Modification du layout
		memoryZone.setLayout(new GridLayout(2, 1));
		//Modification de la dimension de la zone mémoire
		memoryZone.setPreferredSize(new Dimension(125, 70));
		
		//Modification graphique du JLabel contenant le nom de la zone mémoire 
		title.setOpaque(true);
		title.setBackground(titleColor);
		title.setHorizontalAlignment(SwingConstants.CENTER);
		title.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		//Modification graphique du contenu de la zone mémoire 
		content.setOpaque(true);
		content.setBackground(contentColor);  
		content.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		content.setHorizontalAlignment(SwingConstants.CENTER);
		
		//Modification de la couleur, de la taille et de la police du nom de la zone mémoire                         
		Font fonttitle = new Font("Serif", Font.BOLD, 18);
		title.setFont(fonttitle);
		title.setForeground(Color.DARK_GRAY);
		
		//Modification de la couleur, de la taille et de la police du contenu de la zone mémoire   
		Font fontcontent = new Font("Serif", Font.ROMAN_BASELINE, 16);
		content.setFont(fontcontent);
		content.setForeground(Color.DARK_GRAY); 
		
		//Ajout des composants dans le JPanel constituant la zone mémoire
		memoryZone.add(title);
		memoryZone.add(content);
		
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof MemoryZone)) {
			return false;
		}
		MemoryZone b = (MemoryZone) o;
		if (memoryZone != b.getMemoryZone()) {
			return false;
		}
		if (title != b.getTitle()) {
			return false;
		}
		if (content != b.getContent()) {
			return false;
		}
		if (!titleColor.equals(b.getTitleColor())) {
			return false;
		}
		if (!contentColor.equals(b.getContentColor())) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override
	public int hashCode() {
		return super.hashCode() + title.getText().hashCode() + content.getText().hashCode() + titleColor.hashCode()  + contentColor.hashCode();
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Robot memory zone " + getTitle() + " allowing the results of operations entered by the user to be recorded.\n";
	}
	
}
