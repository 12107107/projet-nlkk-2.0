package main.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * Les instances de la classe BoxView constitueront les cases formant une plateforme.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class BoxView extends JPanel {

	private static final long serialVersionUID = -4798990911325931755L;

	private ImageIcon background;
	private JLabel contentBackground;
	private RobotView octopunk;
	private JLabel natureBox;
	private boolean containsRobot;
	private boolean accessible;
	
	
	/**
	 * Initialise une nouvelle instance de la classe BoxView.
	 */
	public BoxView() {
		super();
		containsRobot = false;
		accessible = true;
		background = new ImageIcon(getClass().getResource("Background Platform.jpg"));
		contentBackground = new JLabel(background);
		octopunk = new RobotView();
		natureBox = null;
		setBoxStyle();
	}
	
	/**
	 * Initialise une nouvelle instance de la classe BoxView ayant la couleur spécifiée.
	 * 
	 * @param couleur la couleur de fond de cette case
	 */
	public BoxView(ImageIcon img) {
		super();
		containsRobot = false;
		accessible = true;
		octopunk = new RobotView();
		background = new ImageIcon(getClass().getResource("Background Platform.jpg"));
		contentBackground = new JLabel(background);
		natureBox = null;
		setBoxStyle();
	}
	
	/**
	 * @return le JLabel de cette case
	 * @pure
	 */
	public JLabel getContainingBackground() {
		return contentBackground;
	}
	
	/**
	 * @return l'image contenue dans cette case
	 * @pure
	 */
	public ImageIcon getImageBox() {
		return background;
	}
	
	/**
	 * @return le robot contenu dans cette case
	 * @pure
	 */
	public RobotView getRobot() {
		return octopunk;
	}
	
	/**
	 * @return true si la case contient le robot et false sinon
	 * @pure
	 */
	public boolean containsRobot() {
		return containsRobot;
	}
	
	/**
	 * @return le booléen indiquant si la case est accessible ou non
	 * @pure
	 */
	public boolean isAccessible() {
		return accessible;
	}
	
	/**
	 * @return	le type de la case (normale, contenant le robot ou inaccessible)
	 * @pure
	 */
	public JLabel getNatureOfBox() {
		return natureBox;
	}
	
	/**
	 * Modifie le label de cette case.
	 * 
	 * @param newLabel le nouveau label de cette case
	 */
	public void setLabel(JLabel newLabel) {
		if (natureBox != null) {
			contentBackground.remove(natureBox);
		}
		
		natureBox = newLabel;
		contentBackground.add(natureBox);
	}
	
	/**
	 * Modifie le booléen indiquant si cette case contient le robot.
	 * 
	 * @param containsRobot le nouveau booléen indiquant si cette case contient le robot
	 */
	public void setContainsRobot (boolean containsRobot) {
		this.containsRobot = containsRobot;
	}
	
	/**
	 * Modifie l'image contenue dans cette case.
	 * 
	 * @param newImage la nouvelle image contenue dans cette case
	 */
	public void setImageBackground (ImageIcon newImage) {
		//Redimensionnement de l'image
		Image img = newImage.getImage();  
		Image newimg = img.getScaledInstance((int) this.getPreferredSize().getWidth(), (int) this.getPreferredSize().getHeight(),  Image.SCALE_SMOOTH) ;  
		newImage = new ImageIcon(newimg);
		
		background = newImage;
		contentBackground.setIcon(background);
		setBoxStyle();
	}
	
	/**
	 * Transforme cette case en une case normale.      
	 */
	public void setNormalBox() {
		setContainsRobot(false);
		accessible = true;
		contentBackground.remove(natureBox);
		setBoxStyle();
		
	}
	
	/**
	 * Transforme cette case en une case contenant le robot.
	 */
	public void setRobotBox () {
		setContainsRobot(true);
		accessible = true;
		setLabel(octopunk.getRobotContainer());
		setBoxStyle(); 
	}
	
	/**
	 * Transforme cette case en une case inaccessible.
	 */
	public void setInaccessibleBox() {
		setContainsRobot(false);
		accessible = false;
		
		ImageIcon img = new ImageIcon (getClass().getResource("Cross.png"));    
		
		//Redimensionnement de l'image
		Image img_2 = img.getImage();  
		Image newimg = img_2.getScaledInstance(100, 100,  Image.SCALE_SMOOTH) ;  
		img = new ImageIcon(newimg);
		
		JLabel newLabel = new JLabel(img);
		setLabel(newLabel);
		setBoxStyle(); 
	}
	
	/**
	 * Ensemble des modifications graphique de cette case.
	 */
	public void setBoxStyle() {
		//Modification de la dimension de la case
		this.setPreferredSize(new Dimension(150,100));   
		
		//Modification de l'opacité
		this.setOpaque(false);
		contentBackground.setOpaque(false);
		
		//Ajout de l'image de fond
		this.add(contentBackground);
		
		//Création de l'encadrement et modification des layouts
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		this.setLayout(new GridLayout(1, 1));
		contentBackground.setLayout(new GridLayout(1, 1));
		
		//Modifications à apporter lorsque la case n'est pas une case normale
		if (natureBox != null) {
			getContainingBackground().setHorizontalAlignment(SwingConstants.CENTER);
			getContainingBackground().setPreferredSize(this.getPreferredSize());
			natureBox.setOpaque(false);
		}
		
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof BoxView)) {
			return false;
		}
		BoxView b = (BoxView) o;
		if (background != b.getImageBox()) {
			return false;
		}
		if (contentBackground != b.getContainingBackground()) {
			return false;
		}
		if (!octopunk.equals(b.getRobot())) {
			return false;
		}
		if (natureBox != b.getNatureOfBox()) {
			return false;
		}
		if (containsRobot != b.containsRobot()) {
			return false;
		}
		if (accessible != b.isAccessible()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override 
	public int hashCode() {     
		int code = 1;
		if (isAccessible()) {
			code *= 29;
		}
		if (containsRobot()) {
			code *= 43;
		}
		return code + octopunk.hashCode() * 7;
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Box constituting the platform.\n";
	}
	
}
