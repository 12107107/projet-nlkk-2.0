package main.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * Les instances de la classe Dialog constitueront les fenêtres de dialogue transmettant les erreurs repérées dans le code de l'utilisateur.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class Dialog extends JDialog {

	private static final long serialVersionUID = 1L;
	
	private GUIGame window;
	private JTextField dialog;
	
	
	/**
	 * Initialise une nouvelle instance de la classe Dialog.
	 * 
	 * @param w		la fenêtre du jeu
	 * @param text	le texte contenu dans la fenêtre de dialogue
	 */
	public Dialog(GUIGame w, String text) {
		super(w, "Warning", false);     
		window = w;
		dialog = new JTextField(text);
		setDialogStyle();
	}

	/**
	 * @return	la zone de texte de la fenêtre de dialogue
	 * @pure
	 */
	public JTextField getDialog() {
		return dialog;
	}

	/**
	 * Modifie le contenu de la fenêtre de dialogue.
	 * 
	 * @param newText le nouveau texte contenu dans la fenêtre de dialogue
	 */
	public void setDialog(String newText) {
		dialog.setText(newText);;
	}

	/**
	 * @return	la fenêtre du jeu
	 * @pure
	 */
	public JFrame getWindow() {
		return window;
	}
	
	/**
	 * Ensemble des modifications graphique de la fenêtre de dialogue.
	 */
	public void setDialogStyle() {
		//Ajout du texte à afficher dans la fenêtre de dialogue
		this.add(dialog);
		//Définit la dimension de la fenêtre de dialogue
		this.setSize(new Dimension(900, 500));
		//Empêche l'utilisateur de changer la taille de la fenêtre de dialogue
		this.setResizable(false);
		//Positionnement dans la fenêtre du jeu
		this.setBounds(window.getWidth() / 2 - this.getWidth() / 2, window.getHeight() / 2 - this.getHeight() / 2 + 30, this.getWidth(), this.getHeight());
		this.setVisible(true);
		
		//Modification de la couleur de fond
		this.getContentPane().setBackground(Color.DARK_GRAY);
		
		//Empêche la modification du contenu de la fenêtre de dialogue
		dialog.setEditable(false);
		//Aligne le contenu au centre de la fenêtre de dialogue
		dialog.setHorizontalAlignment(SwingConstants.CENTER);
		
		//Modification de l'encadrement
		dialog.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
		dialog.setOpaque(false);
		//Modification de la police, de la taille et de la couleur du texte contenu dans la fenêtre de dialogue
		Font font = new Font("Serif", Font.BOLD, 18);
		dialog.setFont(font);
		dialog.setForeground(new Color(37,159,175));
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Dialog)) {
			return false;
		}
		Dialog b = (Dialog) o;
		if (window != b.getWindow()) {
			return false;
		}
		if (dialog != b.getDialog()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override 
	public int hashCode() {     
		return window.hashCode() * 767;
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Dialog window transmitting errors identified in the user's code.\n";
	}
	
}
