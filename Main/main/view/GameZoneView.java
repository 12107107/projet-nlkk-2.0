package main.view;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.NoSuchElementException;

import javax.swing.JPanel;

/**
 * Les instances de la classe GameZoneView constitueront la zone de jeu dans laquelle le robot se déplacera.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class GameZoneView extends JPanel {
	
	private static final long serialVersionUID = 7157834132516187522L;
	
	private PlatformView p1;
	private PlatformView p2;
	private PlatformView p3;
	private Door door1;
	private Door door2;
	private boolean robotAlive;
	private ControlZone controlZone;
	
	/**
	 * Initialise une nouvelle instance de la classe GameZoneView.
	 * 
	 * @param Czone la zone de code du jeu dans laquelle l'utilisateur entre son code
	 */
	public GameZoneView (ControlZone Czone) {
		super();
		
		p1 = new PlatformView("HOME");
		p2 = new PlatformView("INBOX");
		p3 = new PlatformView("OUTBOX");
		
		door1 = new Door();
		door2 = new Door();
		
		robotAlive = true;
		controlZone = Czone;
		
		setGameZoneStyle();
		
	}
	
	/**
	 * @return	la zone dans laquelle l'utilisateur entre son code
	 * @pure
	 */
	public ControlZone getControlZone() {
		return controlZone;
	}

	/**
	 * @return	le statut du robot
	 * @pure
	 */
	public boolean isRobotAlive() {
		return robotAlive;
	}
	
	/**
	 * Met à jour le statut du robot.
	 * 
	 * @param robotAlive le nouveau statut du robot
	 */
	public void setRobotAlive(boolean robotAlive) {
		this.robotAlive = robotAlive;
	}

	/**
	 * @param i	le numéro de la plateforme voulue
	 * @return	la plateforme i de la zone de jeu
	 * @throws IllegalArgumentException si l'entier donné ne correspond à aucune plateforme
	 */
	public PlatformView getPlatform (int i) {
		if (i == 1) {
			return p1;
		}
		else if (i == 2) {
			return p2;
		}
		else if (i == 3) {
			return p3;
		}
		
		throw new IllegalArgumentException("The given number does not correspond to any platform.");
	}
	
	/**
	 * @param i	le numéro de la porte voulue
	 * @return	la porte i de la zone de jeu
	 * @throws IllegalArgumentException si l'entier donné ne correspond à aucune porte
	 */
	public Door getDoor (int i) {
		if (i == 1) {
			return door1;
		}
		else if (i == 2) {
			return door2;
		}
		
		throw new IllegalArgumentException("The given number does not correspond to any door.");
	}
	
	/**
	 * @return	la case contenant le robot
	 * @throws NoSuchElementException si aucune case de la zone de jeu ne contient le robot
	 */
	public BoxView getBoxContainingRobot() {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (p1.getBoxPlatform(i, j).containsRobot() == true) {    
					return p1.getBoxPlatform(i, j);
				}
				else if (p2.getBoxPlatform(i, j).containsRobot() == true) {
					return p2.getBoxPlatform(i, j);
				}
				else if (p3.getBoxPlatform(i, j).containsRobot() == true) {
					return p3.getBoxPlatform(i, j);
				}
			}
		}
		throw new NoSuchElementException("No box contains the robot.");
	}
	
	/**
	 * @return	la plateforme contenant le robot
	 * @throws NoSuchElementException si aucune plateforme de la zone de jeu ne contient le robot
	 */
	public JPanel getPlatformContainingRobot() {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				if (p1.getBoxPlatform(i, j).containsRobot() == true) {
					return p1.getPlatform();
				}
				else if (p2.getBoxPlatform(i, j).containsRobot() == true) {
					return p2.getPlatform();
				}
				if (p3.getBoxPlatform(i, j).containsRobot() == true) {
					return p3.getPlatform();
				}
			}
		}
		throw new NoSuchElementException("No platform contains the robot.");
	}
	
	/**
	 * Modifie l'emplacement du robot dans la zone de jeu.
	 * 
	 * @param p	le numéro de la plateforme dans laquelle on souhaite déplacer le robot
	 * @param i	le numéro de ligne de la case dans laquelle on souhaite déplacer le robot 
	 * @param j	le numéro de colonne de la case dans laquelle on souhaite déplacer le robot 
	 * @throws IllegalArgumentException si le numéro de plateforme ou de case donné ne correspond à aucune plateforme ou aucune case de la zone de jeu
	 */
	public void setRobotLocation (int p, int i, int j) { 
		PlatformView tmp;
		if (p == 1) {
			tmp = p1;
		}
		else if (p == 2) {
			tmp = p2;
		}
		else if (p == 3) {
			tmp = p3;
		}
		else {
			throw new IllegalArgumentException("The given platform or box number does not correspond to any platform or box.");
		}
		if (tmp.getBoxPlatform(i, j).isAccessible()) {
			//Rétablit le style de la case qui contenait le robot et transforme la case choisit en case robot
			getBoxContainingRobot().setNormalBox();
			tmp.getBoxPlatform(i, j).setRobotBox();
			
		}
	}
	
	/**
	 * Fait disparaître le robot de la zone de jeu. Le robot est alors considéré comme mort.
	 */
	public void killRobot() {
		setRobotAlive(false);
		getBoxContainingRobot().setNormalBox();   
		controlZone.Blocked(false);
		controlZone.getTextZone().revalidate();
		this.revalidate();
	}
	
	/**
	 * Ensemble des modifications graphique de la zone de jeu.
	 */
	public void setGameZoneStyle() {
		//Modification du layout de la zone de jeu pour que les éléments de la zone de jeu soient collés et positionnés en ligne 
		this.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
		//Modification de la dimension de la zone de jeu
		this.setPreferredSize(new Dimension((int) p1.getPlatform().getPreferredSize().getWidth() + (int) door1.getPreferredSize().getWidth() + (int) p2.getPlatform().getPreferredSize().getWidth() + (int) door2.getPreferredSize().getWidth() + (int) p3.getPlatform().getPreferredSize().getWidth(), (int) p1.getPlatform().getPreferredSize().getHeight()));
		//Modification de l'opacité
		this.setOpaque(false);
		
		//Ajout des éléments qui constituent notre zone de jeu
		this.add(p1.getPlatform());
		this.add(door1);
		this.add(p2.getPlatform());
		this.add(door2);
		this.add(p3.getPlatform());
		
		//Ajout du robot sur la place qu'il prendra toujours au début du jeu
		p1.getBoxPlatform(0, 0).setRobotBox();
		
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof GameZoneView)) {
			return false;
		}
		GameZoneView b = (GameZoneView) o;
		if (!(p1.equals(b.getPlatform(1)))) {
			return false;
		}
		if (!(p2.equals(b.getPlatform(2)))) {
			return false;
		}
		if (!(p3.equals(b.getPlatform(3)))) {
			return false;
		}
		if (!(door1.equals(b.getDoor(1)))) {
			return false;
		}
		if (!(door2.equals(b.getDoor(2)))) {
			return false;
		}
		if (robotAlive != b.isRobotAlive()) {
			return false;
		}
		if (!controlZone.equals(b.getControlZone())) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override
	public int hashCode() {
		return p1.hashCode() * 53 + p2.hashCode() * 41 + p3.hashCode() * 73 + door1.hashCode() * 83 + door2.hashCode() * 97 + (robotAlive == true ? 19 : 419) + controlZone.hashCode();
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Game zone on which the robot moves. This zone is made up of three platforms and two doors.\n";
	}
	
}
