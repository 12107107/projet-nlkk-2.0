package main.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

/**
 * Les instances de la classe ControlZone constitueront la zone dans laquelle l'utilisateur entre son code.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class ControlZone extends JScrollPane {

	private static final long serialVersionUID = 1337184703184887463L;
	
	private JTextArea textZone;
	private boolean isBlocked;
	
	/**
	 * Initialise une nouvelle instance de la classe ControlZone.
	 */
	public ControlZone () {
		super();
		isBlocked = false;
		textZone = new JTextArea(); 
		this.getViewport().add(textZone);
		setControlZoneStyle();
	}
	
	/**
	 * @return	la zone dans laquelle l'utilisateur entre son code
	 * @pure
	 */
	public JTextArea getTextZone() {
		return textZone;
	}
	
	/**
	 * @return	le booléen indiquant si la zone dans laquelle l'utilisateur entre son code est bloqué ou non
	 * @pure
	 */
	public boolean isBlocked() {
		return isBlocked;
	}
	
	/**
	 * Bloque ou débloque la zone dans laquelle l'utilisateur entre son code.
	 * 
	 * @param blocked le booléen indiquant si la zone dans laquelle l'utilisateur entre son code doit être bloqué ou non
	 */
	public void Blocked (boolean blocked) {   
		getTextZone().setEditable(!blocked);
		isBlocked = blocked;
	}
	
	/**
	 * Ensemble des modifications graphique de la zone de contrôle.
	 */
	public void setControlZoneStyle() {  
		//Revenir à la ligne lorqu'on arrive à la fin de l'actuelle
		textZone.setLineWrap(true);
		
		//Modifie la dimension de la zone de code
		this.setPreferredSize(new Dimension(300, 300));
		
		//Afficher seulement la barre de défilement verticale
		this.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		//Changement de la couleur du fond
		textZone.setOpaque(true);
		textZone.setBackground(Color.DARK_GRAY);
		
		//Changement de la couleur du curseur
		textZone.setCaretColor(new Color(37,159,175));
		
		//Changement de la couleur du texte
		textZone.setForeground(new Color(37,159,175));
		
		//Changement de la police et de la taille du texte
		Font font = new Font("Serif", Font.BOLD, 14);  
		textZone.setFont(font);
		
		//Changement de la couleur du fond de la barre de défilement
		this.getVerticalScrollBar().setBackground(new Color(37,159,175));
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ControlZone)) {
			return false;
		}
		ControlZone b = (ControlZone) o;
		if (textZone != b.getTextZone()) {
			return false;
		}
		if (isBlocked != b.isBlocked()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override
	public int hashCode() {
		int code = 1;
		if (isBlocked) {
			code *= 71;
		}
		return code + textZone.getText().hashCode();
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Control zone allowing the user to enter assembly code.\n";
	}
	
}
