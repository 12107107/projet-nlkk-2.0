package main.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * Les instances de la classe ButtonView constitueront les boutons contenus dans la fenêtre du jeu.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class ButtonView extends JButton {   
	
	private static final long serialVersionUID = -4229606153296814900L;
	
	private String text;
	private Color color;
	private ImageIcon icon; 
	
	/**
	 * Initialise une nouvelle instance de la classe ButtonView.
	 */
	public ButtonView() {
		super();
		text = null;
		color = new Color(37,159,175);
		icon = null;
		setButtonStyle();
	}
	
	/**
	 * Initialise une nouvelle instance de la classe ButtonView ayant le texte spécifié.
	 * 
	 * @param text le texte contenu dans le bouton
	 */
	public ButtonView (String text) {
		super(text);
		this.text = text;
		icon = null;
		color = new Color(37,159,175);
		setButtonStyle();
	}
	
	/**
	 * Initialise une nouvelle instance de la classe ButtonView ayant l'icone spécifiée.
	 * 
	 * @param icon l'icone contenu dans le bouton
	 */
	public ButtonView (ImageIcon icon) {
		super(icon);
		setButtonStyle();
		
		//Redimensionnement de l'image
		Image img = icon.getImage();  
		Image newimg = img.getScaledInstance((int) this.getPreferredSize().getWidth(), (int) this.getPreferredSize().getHeight(),  Image.SCALE_SMOOTH) ;  
		this.icon = new ImageIcon(newimg);
		
		this.setIcon(this.icon);
		
		color = null;
		text = null;
		
	}
	
	/**
	 * @return	le texte contenu dans le bouton
	 * @pure
	 */
	public String getButtonText() {
		return text;
	}
	
	/**
	 * @return	la couleur du bouton
	 * @pure
	 */
	public Color getButtonColor() {
		return color;
	}
	
	/**
	 * @return	l'icone du bouton
	 * @pure
	 */
	public ImageIcon getButtonIcon() {
		return icon;
	}
	
	/**
	 * Modifie l'icone contenue dans le bouton.
	 * 
	 * @param newIcon la nouvelle icone du bouton
	 */
	public void setIconButton (ImageIcon newIcon) {
		//Redimensionnement de l'image
		Image img = newIcon.getImage();  
		Image newimg = img.getScaledInstance((int) this.getPreferredSize().getWidth(), (int) this.getPreferredSize().getHeight(),  Image.SCALE_SMOOTH) ;  
		icon = new ImageIcon(newimg);
		this.setIcon(icon);
	}
	
	/**
	 * Modifie la couleur du bouton.
	 * 
	 * @param newColor la nouvelle couleur du bouton
	 */
	public void setColorButton (Color newColor) {
		this.setBackground(newColor);
		color = newColor;
	}
	
	/**
	 * Modifie le texte contenu dans le bouton.
	 * 
	 * @param newText le nouveau texte du bouton
	 */
	public void setTextButton (String newText) {
		this.setText(newText);
		text = newText;
	}
	
	/**
	 * Ensemble des modifications graphique du bouton.
	 */
	public void setButtonStyle() {
		//Modification de la dimension et du layout du bouton
		this.setPreferredSize(new Dimension(200, 100));
		this.setLayout(new GridLayout(1,1));
		this.setOpaque(true);
		// Changement de la couleur du fond
		this.setBackground(color); 
		//Enlèvement de l'encadrement autour du texte  
		this.setFocusPainted(false);
		//Changement de la couleur et le style du texte   
		this.setForeground(Color.DARK_GRAY);
		Font font = new Font("Serif", Font.ITALIC, 16);
		this.setFont(font);
		//Changement de la taille du this
		this.setPreferredSize(new Dimension(125, 70));
		//Coloration du bord du bouton
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof ButtonView)) {
			return false;
		}
		ButtonView b = (ButtonView) o;
		if (text != b.getButtonText()) {
			return false;
		}
		if (color != b.getButtonColor()) {
			return false;
		}
		if (icon != b.getButtonIcon()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override
	public int hashCode() {
		return text.hashCode() * 13 + color.hashCode() * 5;
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Button " + getButtonText() + " allowing the user to perform an action.\n";
	}
	
}
