package main.view;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JPanel;

import main.model.PlatformModel;

/**
 * Les instances de la classe PlatformView constitueront les plateformes de la zone de jeu sur lesquelles se déplacera le robot.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class PlatformView extends PlatformModel {
	
	private JPanel platform;

	private BoxView _0_0; 
	private BoxView _0_1;
	private BoxView _0_2;
	private BoxView _0_3;
	private BoxView _0_4;
	
	private BoxView _1_0;
	private BoxView _1_1;
	private BoxView _1_2;
	private BoxView _1_3;
	private BoxView _1_4;
	
	private BoxView _2_0;
	private BoxView _2_1;
	private BoxView _2_2;
	private BoxView _2_3;
	private BoxView _2_4;
	
	private BoxView _3_0;
	private BoxView _3_1;
	private BoxView _3_2;
	private BoxView _3_3;
	private BoxView _3_4;
	
	private BoxView _4_0;
	private BoxView _4_1;
	private BoxView _4_2;
	private BoxView _4_3;
	private BoxView _4_4;
	
	/**
	 * Initialise une nouvelle instance de la classe PlatformView.
	 * 
	 * @param name le nom de la plateforme
	 */
	public PlatformView (String name) {
		super(name);
		
		platform = new JPanel();
		
		//Modification du layout et de la dimension de cette plateforme
		platform.setLayout(new GridLayout(5, 5));
		platform.setPreferredSize(new Dimension(300, 300));  
		
		_0_0 = new BoxView();
		_0_1 = new BoxView();
		_0_2 = new BoxView();
		_0_3 = new BoxView();
		_0_4 = new BoxView();
		
		_1_0 = new BoxView();
		_1_1 = new BoxView();
		_1_2 = new BoxView();
		_1_3 = new BoxView();
		_1_4 = new BoxView();
		
		_2_0 = new BoxView();
		_2_1 = new BoxView();
		_2_2 = new BoxView();
		_2_3 = new BoxView();
		_2_4 = new BoxView();
		
		_3_0 = new BoxView();
		_3_1 = new BoxView();
		_3_2 = new BoxView();
		_3_3 = new BoxView();
		_3_4 = new BoxView();
		
		_4_0 = new BoxView();
		_4_1 = new BoxView();
		_4_2 = new BoxView();
		_4_3 = new BoxView();
		_4_4 = new BoxView();
		
		//Ajout des cases dans la plateforme
		platform.add(_0_0);
		platform.add(_0_1);
		platform.add(_0_2);
		platform.add(_0_3);
		platform.add(_0_4);
		
		platform.add(_1_0);
		platform.add(_1_1);
		platform.add(_1_2);
		platform.add(_1_3);
		platform.add(_1_4);
		
		platform.add(_2_0);
		platform.add(_2_1);
		platform.add(_2_2);
		platform.add(_2_3);
		platform.add(_2_4);
		
		platform.add(_3_0);
		platform.add(_3_1);
		platform.add(_3_2);
		platform.add(_3_3);
		platform.add(_3_4);
		
		platform.add(_4_0);
		platform.add(_4_1);
		platform.add(_4_2);
		platform.add(_4_3);
		platform.add(_4_4);
		
		platform.setOpaque(false);
	
	}
	
	/**
	 * @return	le contenant de la plateforme
	 * @pure
	 */
	public JPanel getPlatform() {
		return platform;
	}
	
	/**
	 * @param i	le numéro de ligne de la case voulue
	 * @param j	le numéro de colonne de la case voulue
	 * @return	la case dont le numéro de ligne est l'entier i et le numéro de colonne est l'entier j
	 * @throws IllegalArgumentException	si la case dont le numéro de ligne est l'entier i et le numéro de colonne est l'entier j n'existe pas
	 */
	public BoxView getBoxPlatform(int i, int j) {
		
		if (i == 0) {
		
			if (j == 0) {
				return _0_0;
			}
			else if (j == 1) {
				return _0_1;
			}
			else if (j == 2) {
				return _0_2;
			}
			else if (j == 3) {
				return _0_3;
			}
			else if (j == 4) {
				return _0_4;
			}
		}
		
		if (i == 1) {
			
			if (j == 0) {
				return _1_0;
			}
			else if (j == 1) {
				return _1_1;
			}
			else if (j == 2) {
				return _1_2;
			}
			else if (j == 3) {
				return _1_3;
			}
			else if (j == 4) {
				return _1_4;
			}
		}
		
		if (i == 2) {
			
			if (j == 0) {
				return _2_0;
			}
			else if (j == 1) {
				return _2_1;
			}
			else if (j == 2) {
				return _2_2;
			}
			else if (j == 3) {
				return _2_3;
			}
			else if (j == 4) {
				return _2_4;
			}
		}
		
		if (i == 3) {
			
			if (j == 0) {
				return _3_0;
			}
			else if (j == 1) {
				return _3_1;
			}
			else if (j == 2) {
				return _3_2;
			}
			else if (j == 3) {
				return _3_3;
			}
			else if (j == 4) {
				return _3_4;
			}
		}
		
		if (i == 4) {
			
			if (j == 0) {
				return _4_0;
			}
			else if (j == 1) {
				return _4_1;
			}
			else if (j == 2) {
				return _4_2;
			}
			else if (j == 3) {
				return _4_3;
			}
			else if (j == 4) {
				return _4_4;
			}
		}

		throw new IllegalArgumentException("The box you entered doesn't exist.");
		
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof PlatformView)) {
			return false;
		}
		PlatformView b = (PlatformView) o;
		if (platform != b.getPlatform()) {
			return false;
		}
		if (!_0_0.equals(b.getBoxPlatform(0, 0))) {
			return false;
		}
		if (!_0_1.equals(b.getBoxPlatform(0, 1))) {
			return false;
		}
		if (!_0_2.equals(b.getBoxPlatform(0, 2))) {
			return false;
		}
		if (!_0_3.equals(b.getBoxPlatform(0, 3))) {
			return false;
		}
		if (!_0_4.equals(b.getBoxPlatform(0, 4))) {
			return false;
		}
		
		if (!_1_0.equals(b.getBoxPlatform(1, 0))) {
			return false;
		}
		if (!_1_1.equals(b.getBoxPlatform(1, 1))) {
			return false;
		}
		if (!_1_2.equals(b.getBoxPlatform(1, 2))) {
			return false;
		}
		if (!_1_3.equals(b.getBoxPlatform(1, 3))) {
			return false;
		}
		if (!_1_4.equals(b.getBoxPlatform(1, 4))) {
			return false;
		}
		
		if (!_2_0.equals(b.getBoxPlatform(2, 0))) {
			return false;
		}
		if (!_2_1.equals(b.getBoxPlatform(2, 1))) {
			return false;
		}
		if (!_2_2.equals(b.getBoxPlatform(2, 2))) {
			return false;
		}
		if (!_2_3.equals(b.getBoxPlatform(2, 3))) {
			return false;
		}
		if (!_2_4.equals(b.getBoxPlatform(2, 4))) {
			return false;
		}
		
		if (!_3_0.equals(b.getBoxPlatform(3, 0))) {
			return false;
		}
		if (!_3_1.equals(b.getBoxPlatform(3, 1))) {
			return false;
		}
		if (!_3_2.equals(b.getBoxPlatform(3, 2))) {
			return false;
		}
		if (!_3_3.equals(b.getBoxPlatform(3, 3))) {
			return false;
		}
		if (!_3_4.equals(b.getBoxPlatform(3, 4))) {
			return false;
		}
		
		if (!_4_0.equals(b.getBoxPlatform(4, 0))) {
			return false;
		}
		if (!_4_1.equals(b.getBoxPlatform(4, 1))) {
			return false;
		}
		if (!_4_2.equals(b.getBoxPlatform(4, 2))) {
			return false;
		}
		if (!_4_3.equals(b.getBoxPlatform(4, 3))) {
			return false;
		}
		if (!_4_4.equals(b.getBoxPlatform(4, 4))) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override
	public int hashCode() {
		int code = 0;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				code += getBoxPlatform(i, j).hashCode() * 101;
			}
		}
		return code;
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Platform forming part of the game zone on which the robot moves.\n";
	}
	
}	
