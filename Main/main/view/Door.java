package main.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * Les instances de la classe Door constitueront les portes donnant accès aux plateformes de la zone de jeu.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class Door extends JPanel {

	private static final long serialVersionUID = -2266731111276067916L;
	
	private JLabel background;
	private ImageIcon backImage;
	private JLabel doorNumber;
	private JLabel returnNumber;
	
	/**
	 * Initialise une nouvelle instance de la classe Door.
	 */
	public Door() {
		super();
		doorNumber = new JLabel("800");
		returnNumber = new JLabel("-1");
		backImage = new ImageIcon(getClass().getResource("Background Door.jpg"));
		background = new JLabel (backImage);
		
		setDoorStyle();
	}
	
	
	/**
	 * @return	le contenant de l'image de fond de la porte
	 * @pure
	 */
	public JLabel getBackgroundDoor() {
		return background;
	}

	/**
	 * @return	l'image de fond de la porte
	 * @pure
	 */
	public ImageIcon getBackgroundImage() {
		return backImage;
	}

	/**
	 * @return	le contenant du numéro permettant au robot de se déplacer dans la plateforme suivante
	 * @pure
	 */
	public JLabel getDoorNumber() {
		return doorNumber;
	}

	/**
	 * @return	le contenant du numéro permettant au robot de retourner dans la plateforme précédente
	 * @pure
	 */
	public JLabel getReturnNumber() {
		return returnNumber;
	}

	/**
	 * Ensemble des modifications graphique de la porte.
	 */
	public void setDoorStyle() {
		//Modification de la dimension de la porte
		this.setPreferredSize(new Dimension(150,100));   
		
		//Modification de la police et de la taille du texte 
		Font font = new Font("Serif", Font.BOLD, 18);
		doorNumber.setFont(font);
		returnNumber.setFont(font);
		
		//Modification de l'alignement du texte 
		doorNumber.setHorizontalAlignment(SwingConstants.CENTER);
		returnNumber.setHorizontalAlignment(SwingConstants.CENTER);
		
		//Modification de la taille du contenant du numéro permettant au robot de retourner dans la plateforme précédente
		returnNumber.setPreferredSize(doorNumber.getPreferredSize());
		
		//Modification de l'opacité des contenants des numéros permettant au robot de se déplacer entre les différentes plateformes
		doorNumber.setOpaque(false);
		returnNumber.setOpaque(false);
		
		//Ajouts des différents éléments constituant la porte et modification de leur layout
		this.add(background);
		background.setLayout(new BorderLayout());
		background.add(doorNumber, BorderLayout.WEST);
		background.add(returnNumber, BorderLayout.EAST);
		this.setLayout(new GridLayout(1, 1));
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Door)) {
			return false;
		}
		Door b = (Door) o;
		if (background != b.getBackgroundDoor()) {
			return false;
		}
		if (backImage != b.getBackgroundImage()) {
			return false;
		}
		if (doorNumber != b.getDoorNumber()) {
			return false;
		}
		if (returnNumber != b.getReturnNumber()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override
	public int hashCode() {
		return doorNumber.getText().hashCode() + returnNumber.getText().hashCode();
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Door allowing the robot to move between the different platforms in the game zone.\n";
	}

}
