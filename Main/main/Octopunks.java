package main;

import main.controller.GameController;
import main.view.GUIGame;

public class Octopunks {

	public static void main(String[] args) {
		//Initialisation de la fenêtre du jeu
		GUIGame GUI = new GUIGame();
		//Initialisation des contrôleurs de la fenêtre du jeu
		GameController controller = new GameController(GUI);
		//Rend la fenêtre visible et indique le commencement du jeu
		controller.startGame();
	}

}
