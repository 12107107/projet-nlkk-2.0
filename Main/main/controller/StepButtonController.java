package main.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.NoSuchElementException;
import java.util.Random;

import main.model.Instruction;
import main.view.ButtonView;
import main.view.ControlZone;
import main.view.Dialog;
import main.view.GUIGame;
import main.view.GameZoneView;
import main.view.MemoryZone;
import main.view.RobotView;


/**
 * Les instances de la classe StepButtonController définissent les actions qu'exécutera le bouton step de la fenêtre du jeu.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class StepButtonController implements ActionListener {
	
	private ButtonView step;
	private RobotView octopunk;   
	private ControlZone controlZone;
	private GameZoneView gameZone;
	private MemoryZone memoryZoneX;
	private MemoryZone memoryZoneT;
	private GUIGame gui;
	private boolean instrPassed;
	private boolean inAction;
	private boolean exception;
	
	
	/**
	 *  Initialise une nouvelle instance de la classe StepButtonController qui lance l'exécution d'une des instructions entrées par l'utilisateur.
	 * 
	 * @param gui		la fenêtre du jeu
	 * @param robot		le robot du jeu
	 * @param button	le bouton step du jeu
	 * @param cZone		la zone de code dans laquelle l'utilisateur entre des instructions
	 * @param gZone		la zone de jeu dans laquelle le robot se déplace
	 * @param X			la zone mémoire X du robot
	 * @param T			la zone mémoire T du robot
	 */
	public StepButtonController (GUIGame gui, RobotView robot, ButtonView button, ControlZone cZone, GameZoneView gZone, MemoryZone X, MemoryZone T) {
		this.gui = gui;
		this.step = button;
		controlZone = cZone;
		gameZone = gZone;
		this.octopunk = robot;
		memoryZoneX = X;
		memoryZoneT = T;
		instrPassed = false;
		exception = false;
		//Ajout de l'ActionListener
		step.addActionListener(this);
	}
	
	/**
	 * @return	la fenêtre du jeu
	 * @pure
	 */
	public GUIGame getGUI() {
		return gui;
	}

	/**
	 * @return	le bouton step
	 * @pure
	 */
	public ButtonView getStep() {
		return step;
	}

	/**
	 * @return	le robot
	 * @pure
	 */
	public RobotView getOctopunk() {
		return octopunk;
	}

	/**
	 * @return	la zone de code dans laquelle l'utilisateur entre son code
	 * @pure
	 */
	public ControlZone getControlZone() {
		return controlZone;
	}

	/**
	 * @return	la zone de jeu dans laquelle le robot se déplace
	 * @pure
	 */
	public GameZoneView getGameZone() {
		return gameZone;
	}

	/**
	 * @return	la zone mémoire X du robot
	 * @pure
	 */
	public MemoryZone getMemoryZoneX() {
		return memoryZoneX;
	}

	/**
	 * @return	la zone mémoire T du robot
	 * @pure
	 */
	public MemoryZone getMemoryZoneT() {
		return memoryZoneT;
	}

	/**
	 * @return	le booléen indiquant si les instructions entrées par l'utilisateur ont été envoyées au robot ou si elles ont déjà été exécutées
	 * @pure
	 */
	public boolean isInstructionsPassed() {
		return instrPassed;
	}
	
	/**
	 * @return	le booléen indiquant l'état du bouton
	 * @pure
	 */
	public boolean isInAction() {
		return inAction;
	}
	
	/**
	 * @return	le booléen indiquant si une exception a été reçue
	 * @pure
	 */
	public boolean getException() {
		return exception;
	}
	
	/**
	 * Définit les actions à exécuter lorsque l'utilisateur clique sur le bouton step.
	 */
	@Override
	public void actionPerformed (ActionEvent e) {
		if (gui.isButtonInAction() == false) {
			inAction = true;
			gui.setButtonInAction(true);
			
			//Blocage de la zone de code
			controlZone.Blocked(true);
			
			//Envoie des instructions pour le parsing
			if (instrPassed == false || gameZone.isRobotAlive() == false) { 
				passInstructions();
			}
			
			//Vérifie qu'une exception n'a pas été reçue lors de l'envoi du code entré par l'utilisateur au robot
			if (exception == true) {
				return;
			}
			
			//Exécution d'une instruction entrée par l'utilisateur
			try {
				octopunk.exec();
			}
			catch (IllegalArgumentException i) {
				new Dialog(gui, i.getMessage());
				gameZone.killRobot();
				setInstrPassedToFalse();
				setInActionToFalse();
				gui.setButtonInAction(false);
				return;
			}
			catch (NoSuchElementException n) {
				new Dialog(gui, n.getMessage());
				gameZone.killRobot();
				setInstrPassedToFalse();
				setInActionToFalse();
				gui.setButtonInAction(false);
				return;
			}
			catch (InternalError r) {
				new Dialog(gui, r.getMessage());
				gameZone.killRobot();
				setInstrPassedToFalse();
				setInActionToFalse();
				gui.setButtonInAction(false);
				return;
			}
			catch (UnsupportedOperationException u) {
				new Dialog(gui, u.getMessage());
				gameZone.killRobot();
				setInstrPassedToFalse();
				setInActionToFalse();
				gui.setButtonInAction(false);
				return;
			}
			
			//Conversion des valeurs entières contenues dans les registres X, T du RobotModel en String
			String X = Integer.toString(octopunk.getXValue());
			String T = Integer.toString(octopunk.getTValue());
			//Mise à jour des zones mémoires X, T
			memoryZoneX.getContent().setText(X);
			memoryZoneT.getContent().setText(T);
			//Génère des nombres aléatoires pour les déplacements du robot
			int i = numberGenerator();
			int j = numberGenerator();
			//Déplacement du robot si l'instruction exécutée est LINK
			Instruction instruExecute = octopunk.getLastCalledInstruction();
			//Tue le robot si l'instruction executée est HALT
			if (instruExecute.equals(new Instruction("HALT"))) {
				gameZone.killRobot();
				setInstrPassedToFalse();
				setInActionToFalse();
			}
			//Déplacement du robot si l'instruction exécutée est LINK
			else if (instruExecute.equals(new Instruction("LINK 800"))) {
				if (gameZone.getPlatformContainingRobot() == gameZone.getPlatform(1).getPlatform()) {  
					gameZone.setRobotLocation(2, i, j);
				}
				else if (gameZone.getPlatformContainingRobot() == gameZone.getPlatform(2).getPlatform()) {
					gameZone.setRobotLocation(3, i, j);
				}
			}
			else if (instruExecute.equals(new Instruction("LINK -1"))) {
				if (gameZone.getPlatformContainingRobot() == gameZone.getPlatform(2).getPlatform()) {
					gameZone.setRobotLocation(1, i, j);
				}
				else if (gameZone.getPlatformContainingRobot() == gameZone.getPlatform(3).getPlatform()) {
					gameZone.setRobotLocation(2, i, j);
				}
			}
			
			gui.setButtonInAction(false);
		
			//Mise à jour
			gameZone.revalidate();
			controlZone.getTextZone().revalidate();
		}
	
	}
	
	/**
	 * Envoi des instructions entrées par l'utilisateur au robot.
	 */
	public void passInstructions() {
		exception = false;
		if (gameZone.isRobotAlive() == false) {
			gameZone.getPlatform(1).getBoxPlatform(0, 0).setRobotBox();
			gameZone.setRobotAlive(true);
		}
		try {
			if (controlZone.getTextZone().getText().isBlank() == false) {
				octopunk.ModifyInstructions(controlZone.getTextZone().getText());
			}
			else {
				octopunk.ModifyInstructions(null);
			}
		}
		catch (IllegalArgumentException e) {
			new Dialog(gui, e.getMessage());
			controlZone.Blocked(false);
			setInstrPassedToFalse();
			gui.setButtonInAction(false);
			exception = true;
			return;
		}
		catch (NullPointerException n) {
			new Dialog(gui, n.getMessage());
			controlZone.Blocked(false);
			setInstrPassedToFalse();
			gui.setButtonInAction(false);
			exception = true;
			return;
		}
		instrPassed = true;
		
	}
	
	/**
	 * Indique que le bouton step n'est plus en action.
	 */
	public void setInActionToFalse() { 
		inAction = false;
	}
	
	/**
	 * Indique que les instructions entrées par l'utilisateur n'ont pas été envoyées au robot ou qu'elles ont délà été exécutées.
	 */
	public void setInstrPassedToFalse() { 
		instrPassed = false;
	}

	/**
	 * @return	un nombre aléatoire entre 0 et 4 pour le déplacement aléatoire du robot
	 */
	public int numberGenerator() {
		Random rand = new Random();
		return rand.nextInt(5);
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof StepButtonController)) {
			return false;
		}
		StepButtonController b = (StepButtonController) o;
		if (!step.equals(b.getStep())) {
			return false;
		}
		if (!octopunk.equals(b.getOctopunk())) {
			return false;
		}
		if (!controlZone.equals(b.getControlZone())) {
			return false;
		}
		if (!gameZone.equals(b.getGameZone())) {
			return false;
		}
		if (!memoryZoneX.equals(b.getMemoryZoneX())) {
			return false;
		}
		if (!memoryZoneT.equals(b.getMemoryZoneT())) {
			return false;
		}
		if (instrPassed != b.isInstructionsPassed()) {
			return false;
		}
		if (inAction != b.isInAction()) {
			return false;
		}
		if (!gui.equals(b.getGUI())) {
			return false;
		}
		if (exception != b.getException()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override 
	public int hashCode() {     
		return step.hashCode() + octopunk.hashCode() + controlZone.hashCode() + gameZone.hashCode() + memoryZoneX.hashCode() + memoryZoneT.hashCode() + (instrPassed == true ? 7 : 13) + (inAction == true ? 2 : 23) + (exception == true ? 137 : 139) + gui.hashCode();
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Step button controller.\n";
	}
	
}
