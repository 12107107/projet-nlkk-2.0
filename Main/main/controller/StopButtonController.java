package main.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.view.ButtonView;
import main.view.ControlZone;
import main.view.GUIGame;
import main.view.GameZoneView;
import main.view.RobotView;

/**
 * Les instances de la classe StopButtonController définissent les actions qu'exécutera le bouton stop de la fenêtre du jeu.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class StopButtonController implements ActionListener {

	private RobotView octopunk;
	private ButtonView stop;
	private ControlZone controlZone;
	private StepButtonController stepController;
	private GameZoneView gameZone;
	private GUIGame gui;
	
	
	/**
	 * Initialise une nouvelle instance de la classe StopButtonController. 
	 * 
	 * @param gui		la fenêtre du jeu
	 * @param robot		le robot
	 * @param stop		le bouton stop de la fenêtre du jeu
	 * @param Czone		la zone de code dans laquelle l'utilisateur entre des instructions
	 * @param step		le contrôleur du bouton step de la fenêtre du jeu
	 * @param Gzone		la zone de jeu de la fenêtre dans laquelle le robot bouge
	 */
	public StopButtonController (GUIGame gui, RobotView robot, ButtonView stop, ControlZone Czone, StepButtonController step, GameZoneView Gzone) {
		this.gui = gui;
		octopunk = robot;
		this.stop = stop;	
		controlZone = Czone;
		this.stepController = step;
		gameZone = Gzone;
		stop.addActionListener(this);
	}
	
	/**
	 * @return	la fenêtre du jeu
	 * @pure
	 */
	public GUIGame getGUI() {
		return gui;
	}
	
	/**
	 * @return	le robot	
	 * @pure
	 */
	public RobotView getRobot() {
		return octopunk;
	}
	
	/**
	 * @return	le bouton stop		
	 * @pure
	 */
	public ButtonView getStop() {
		return stop;
	}

	/**
	 * @return	la zone de code
	 * @pure
	 */
	public ControlZone getControlZone() {
		return controlZone;
	}

	/**
	 * @return	le contrôleur du bouton step du jeu
	 * @pure
	 */
	public StepButtonController getStepController() {
		return stepController;
	}

	/**
	 * @return	la zone de jeu
	 * @pure
	 */
	public GameZoneView getGameZone() {
		return gameZone;
	}

	/**
	 * Définit les actions à effectuer lorsque le bouton stop est cliqué (indique que l'utilisateur veut modifier son code après avoir cliqué sur le bouton step ou qu'il veut repositionner le robot dans la case qu'il occupait au début du jeu).
	 */
	@Override
	public void actionPerformed (ActionEvent e) {
		
		if (gui.isButtonInAction() == false) {
			//Réinitialisation du robot dans sa case de départ
			if (gameZone.isRobotAlive() == true) {
				gameZone.setRobotLocation(1, 0, 0);
			}
			else {
				gameZone.getPlatform(1).getBoxPlatform(0, 0).setRobotBox();
				gameZone.setRobotAlive(true);
			}
			stepController.setInstrPassedToFalse();
			//Déblocage de la zone de code
			controlZone.Blocked(false);
			controlZone.getTextZone().revalidate();
		}
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof StopButtonController)) {
			return false;
		}
		StopButtonController b = (StopButtonController) o;
		if (!stop.equals(b.getStop())) {
			return false;
		}
		if (!stepController.equals(b.getStepController())) {
			return false;
		}
		if (!controlZone.equals(b.getControlZone())) {
			return false;
		}
		if (!gameZone.equals(b.getGameZone())) {
			return false;
		}
		if (!octopunk.equals(b.getRobot())) {
			return false;
		}
		if (!gui.equals(b.getGUI())) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override 
	public int hashCode() {     
		return stop.hashCode() + stepController.hashCode() + controlZone.hashCode() + gameZone.hashCode() + octopunk.hashCode() + gui.hashCode();
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Stop button controller.\n";
	}
	
}
