package main.controller;

import main.view.GUIGame;

/**
 * Les instances de la classe GameController définiront les actions qu'exécuteront les différents boutons de la fenêtre du jeu.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class GameController {
	
	private GUIGame GUI;
	private StepButtonController stepController;
	private StopButtonController stopController;
	private AutoButtonController autoController;
	
	/**
	 * Initialise une nouvelle instance de la classe ControllerGame qui définit les actions à effectuer par les composants de la fenêtre du jeu.
	 * 
	 * @param gui la fenêtre du jeu
	 * @throws NullPointerException	si la fenêtre du jeu spécifiée est nulle
	 */
	public GameController (GUIGame gui) {
		if (gui == null) {
			throw new NullPointerException("The given GUI is null.\n");
		}
		GUI = gui;
		stepController = new StepButtonController (GUI, GUI.getGameZone().getBoxContainingRobot().getRobot(), GUI.getStepButton(), GUI.getConsole(), GUI.getGameZone(), GUI.getXMemoryZone(), GUI.getTMemoryZone());
		autoController = new AutoButtonController(GUI, GUI.getGameZone().getBoxContainingRobot().getRobot(), GUI.getAutoButton(), GUI.getConsole(), GUI.getGameZone(), GUI.getXMemoryZone(), GUI.getTMemoryZone(), stepController);
		stopController = new StopButtonController(GUI, GUI.getGameZone().getBoxContainingRobot().getRobot(), GUI.getStopButton(), GUI.getConsole(), stepController, GUI.getGameZone());
		
	}

	/**
	 * @return	la fenêtre du jeu
	 * @pure
	 */
	public GUIGame getGUI() {
		return GUI;
	}

	/**
	 * @return	le bouton step du jeu
	 * @pure
	 */
	public StepButtonController getStepController() {
		return stepController;
	}

	/**
	 * @return	le bouton stop du jeu
	 * @pure
	 */
	public StopButtonController getStopController() {
		return stopController;
	}
	
	/**
	 * @return	le bouton auto du jeu
	 * @pure
	 */
	public AutoButtonController getAutoController() {
		return autoController;
	}
	
	/**
	 * Rend la fenêtre du jeu visible et indique le début du jeu.
	 */
	public void startGame() {
		GUI.setVisible(true);
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof GameController)) {
			return false;
		}
		GameController b = (GameController) o;
		if (!GUI.equals(b.getGUI())) {
			return false;
		}
		if (!stepController.equals(b.getStepController())) {
			return false;
		}
		if (!stopController.equals(b.getStopController())) {
			return false;
		}
		if (!autoController.equals(b.getAutoController())) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override 
	public int hashCode() {     
		return GUI.hashCode() + stepController.hashCode() + stopController.hashCode() + autoController.hashCode();
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Game window controller.\n";
	}
	
}
