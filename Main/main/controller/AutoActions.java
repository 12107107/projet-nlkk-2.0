package main.controller;

import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import main.model.Instruction;
import main.view.ControlZone;
import main.view.Dialog;
import main.view.GUIGame;
import main.view.GameZoneView;
import main.view.MemoryZone;
import main.view.RobotView;

/**
 * Les instances de la classe AutoActions définissent les actions à effectuer lorsque l'utilisateur clique sur le bouton auto.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class AutoActions extends TimerTask {

	private RobotView octopunk;   
	private ControlZone controlZone;
	private GameZoneView gameZone;
	private MemoryZone memoryZoneX;
	private MemoryZone memoryZoneT;
	private GUIGame gui;
	
	/**
	 * Initialise une nouvelle instance de la classe AutoActions.
	 * 
	 * @param g 		la fenêtre du jeu
	 * @param robot		le robot
	 * @param Czone		la zone dans laquelle l'utilisateur entre son code
	 * @param Gzone		la zone de jeu
	 * @param memX		la zone mémoire X du robot
	 * @param memT		la zone mémoire T du robot
	 */
	public AutoActions(GUIGame g, RobotView robot, ControlZone Czone, GameZoneView Gzone, MemoryZone memX, MemoryZone memT) {
		gui = g;
		octopunk = robot;
		controlZone = Czone;
		gameZone = Gzone;
		memoryZoneX = memX;
		memoryZoneT = memT;
	}
	
	/**
	 * @return	la fenêtre du jeu
	 * @pure
	 */
	public GUIGame getGUI() {
		return gui;
	}
	
	/**
	 * @return	le robot
	 * @pure
	 */
	public RobotView getOctopunk() {
		return octopunk;
	}

	/**
	 * @return	la zone dans laquelle l'utilisateur entre son code
	 * @pure
	 */
	public ControlZone getControlZone() {
		return controlZone;
	}

	/**
	 * @return	la zone de jeu
	 * @pure
	 */
	public GameZoneView getGameZone() {
		return gameZone;
	}

	/**
	 * @return	la zone mémoire X
	 * @pure
	 */
	public MemoryZone getMemoryZoneX() {
		return memoryZoneX;
	}

	/**
	 * @return	la zone mémoire T
	 * @pure
	 */
	public MemoryZone getMemoryZoneT() {
		return memoryZoneT;
	}
	
	/**
	 * Définit les actions à exécuter lorsque l'utilisateur clique sur le bouton auto.
	 */
	@Override
	public void run() {
		//Exécution d'une instruction entrée par l'utilisateur
		try {
			octopunk.exec();
		}
		catch (IllegalArgumentException i) {
			new Dialog(gui, i.getMessage());
			controlZone.Blocked(false);
			gameZone.killRobot();
			gui.setButtonInAction(false);
			cancel();
			return;
		}
		catch (NoSuchElementException n) {
			new Dialog(gui, n.getMessage());
			controlZone.Blocked(false);
			gameZone.killRobot();
			gui.setButtonInAction(false);
			cancel();
			return;
		}
		catch (InternalError r) {
			new Dialog(gui, r.getMessage());
			controlZone.Blocked(false);
			gameZone.killRobot();
			gui.setButtonInAction(false);
			cancel();
			return;
		}
		catch (UnsupportedOperationException u) {
			new Dialog(gui, u.getMessage());
			controlZone.Blocked(false);
			gameZone.killRobot();
			gui.setButtonInAction(false);
			cancel();
			return;
		}
		
		//Conversion des valeurs entières contenues dans les registres X, T du robot en String
		String X = Integer.toString(octopunk.getXValue());
		String T = Integer.toString(octopunk.getTValue());
		//Mise à jour des zones mémoires X, T
		memoryZoneX.getContent().setText(X);
		memoryZoneT.getContent().setText(T);
		//Génère des nombres aléatoires pour les déplacements du robot
		int i = numberGenerator();
		int j = numberGenerator();
		//Déplace le robot si l'instruction exécutée est LINK
		Instruction instruExecute = octopunk.getLastCalledInstruction();
		if (instruExecute.equals(new Instruction("LINK 800"))) {
			if (gameZone.getPlatformContainingRobot() == gameZone.getPlatform(1).getPlatform()) {  
				gameZone.setRobotLocation(2, i, j);
			}
			else if (gameZone.getPlatformContainingRobot() == gameZone.getPlatform(2).getPlatform()) {
				gameZone.setRobotLocation(3, i, j);
			}
		}
		if (instruExecute.equals(new Instruction("LINK -1"))) {
			if (gameZone.getPlatformContainingRobot() == gameZone.getPlatform(2).getPlatform()) {
				gameZone.setRobotLocation(1, i, j);
			}
			else if (gameZone.getPlatformContainingRobot() == gameZone.getPlatform(3).getPlatform()) {
				gameZone.setRobotLocation(2, i, j);
			}
		}
		
		//Mise à jour de la zone de jeu
		gameZone.revalidate();
		
		//Tue le robot si l'instruction executée est HALT
		if (instruExecute.equals(new Instruction("HALT"))) {
			//Fait disparaître le robot.
			gameZone.killRobot();
			//Déblocage de la zone de code
			controlZone.Blocked(false);
			gui.setButtonInAction(false);
			controlZone.revalidate();
			cancel();
		}
		
	}
	
	/**
	 * @return	un nombre aléatoire entre 0 et 4 pour le déplacement aléatoire du robot
	 */
	public int numberGenerator() {
		Random rand = new Random();
		return rand.nextInt(5);
	}
	
	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof AutoActions)) {
			return false;
		}
		AutoActions b = (AutoActions) o;
		if (!octopunk.equals(b.getOctopunk())) {
			return false;
		}
		if (!controlZone.equals(b.getControlZone())) {
			return false;
		}
		if (!gameZone.equals(b.getGameZone())) {
			return false;
		}
		if (!memoryZoneX.equals(b.getMemoryZoneX())) {
			return false;
		}
		if (!memoryZoneT.equals(b.getMemoryZoneT())) {
			return false;
		}
		if (!gui.equals(b.getGUI())) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override 
	public int hashCode() {     
		return octopunk.hashCode() + controlZone.hashCode() + gameZone.hashCode() + memoryZoneX.hashCode() + memoryZoneT.hashCode() + gui.hashCode();
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Actions to perform when user clicks on auto button.\n";
	}
	
}
