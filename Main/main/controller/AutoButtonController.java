package main.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;

import main.view.ButtonView;
import main.view.ControlZone;
import main.view.Dialog;
import main.view.GUIGame;
import main.view.GameZoneView;
import main.view.MemoryZone;
import main.view.RobotView;

/**
 * Les instances de la classe AutoButtonController définissent les actions qu'exécutera le bouton auto de la fenêtre du jeu.
 * 
 * @author HEBRI Nawel 
 * @author MEHIDI Lina
 * @author MADIVANANE Kevin
 * @author MAHTOUT Karim
 */
public class AutoButtonController implements ActionListener {

	private ButtonView auto;
	private RobotView octopunk;   
	private ControlZone controlZone;
	private GameZoneView gameZone;
	private MemoryZone memoryZoneX;
	private MemoryZone memoryZoneT;
	private StepButtonController stepController;
	private GUIGame gui;
	private Timer time;
	
	/**
	 * Initialise une nouvelle instance de la classe AutoButtonController qui lance l'exécution de toutes les instructions entrées par l'utilisateur.
	 * 
	 * @param g			la fenêtre du jeu
	 * @param robot		le robot du jeu
	 * @param button	le bouton auto du jeu 
	 * @param cZone		la zone de code dans laquelle l'utilisateur entre des instructions
	 * @param gZone		la zone de jeu dans laquelle le robot se déplace
	 * @param X			la zone mémoire X du robot
	 * @param T			la zone mémoire T du robot
	 * @param step		le contrôleur du bouton step
	 */
	public AutoButtonController(GUIGame g, RobotView robot, ButtonView button, ControlZone cZone, GameZoneView gZone, MemoryZone X, MemoryZone T, StepButtonController step) {
		gui = g;
		this.stepController = step;
		this.auto = button;
		controlZone = cZone;
		gameZone = gZone;
		this.octopunk = robot;
		memoryZoneX = X;
		memoryZoneT = T;
		time = new Timer();
		//Ajout de l'ActionListener
		auto.addActionListener(this);  
	}
	
	/**
	 * @return	le Timer permettant d'exécuter les instructions les unes à la suite des autres 
	 * @pure
	 */
	public Timer getTimer() {
		return time;
	}
	
	/**
	 * @return	le contrôleur du bouton step
	 * @pure
	 */
	public StepButtonController getStepController() {
		return stepController;
	}
	
	/**
	 * @return	le bouton auto 
	 * @pure
	 */
	public ButtonView getAuto() {
		return auto;
	}

	/**
	 * @return	le robot
	 * @pure
	 */
	public RobotView getOctopunk() {
		return octopunk;
	}

	/**
	 * @return	la zone de code du robot dans laquelle l'utilisateur entre du code
	 * @pure
	 */
	public ControlZone getControlZone() {
		return controlZone;
	}

	/**
	 * @return	la zone de jeu dans laquelle le robot se déplace
	 * @pure
	 */
	public GameZoneView getGameZone() {
		return gameZone;
	}

	/**
	 * @return	la zone mémoire X du robot
	 * @pure
	 */
	public MemoryZone getXMemoryZone() {
		return memoryZoneX;
	}

	/**
	 * @return	la zone mémoire T du robot
	 * @pure
	 */
	public MemoryZone getTMemoryZone() {
		return memoryZoneT;
	}
	
	/**
	 * @return	la fenêtre du jeu
	 * @pure
	 */
	public GUIGame getGUI() {
		return gui;
	}
	
	/**
	 * Définit les actions à effectuer lorsque le bouton auto est cliqué.
	 */
	@Override                                                             
	public void actionPerformed(ActionEvent e) {   
		if (gui.isButtonInAction() == false) {
			gui.setButtonInAction(true);
			//Si l'utilisateur a cliqué sur le bouton step puis sur le bouton auto, met à jour le statut du bouton step 
			if (stepController.isInAction() == true) {
				stepController.setInActionToFalse();
			}
			
			//Blocage de la zone de code
			controlZone.Blocked(true);
			
			//Remet le robot à sa place initiale
			if (gameZone.isRobotAlive() == false) {
				gameZone.getPlatform(1).getBoxPlatform(0, 0).setRobotBox();
				gameZone.setRobotAlive(true);
				gameZone.revalidate();
			}
			else {
				gameZone.setRobotLocation(1, 0, 0);
				gameZone.revalidate();
			}
			
			//Envoi des instructions entrées par l'utilisateur au robot
			try {
				if (!controlZone.getTextZone().getText().isBlank()) {
					octopunk.ModifyInstructions(controlZone.getTextZone().getText());
				}
				else {
					octopunk.ModifyInstructions(null);
				}
			}
			catch (IllegalArgumentException i) {
				new Dialog(gui, i.getMessage());
				controlZone.Blocked(false);
				gui.setButtonInAction(false);
				return;
			}
			catch (NullPointerException n) {
				new Dialog(gui, n.getMessage());
				controlZone.Blocked(false);
				gui.setButtonInAction(false);
				return;
			}
			
			//Permet d'exécuter les instructions de l'utilisateur une par une toutes les 2 secondes
			AutoActions timeTask = new AutoActions(gui, octopunk, controlZone, gameZone, memoryZoneX, memoryZoneT);
			time.schedule(timeTask, 0, 2000);
			
			//Mise à jour de la zone dans laquelle l'utilisateur entre son code
			controlZone.getTextZone().revalidate();
		}
		
	}

	/**
	 * Redéfinition de la méthode equals.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof AutoButtonController)) {
			return false;
		}
		AutoButtonController b = (AutoButtonController) o;
		if (!auto.equals(b.getAuto())) {
			return false;
		}
		if (!octopunk.equals(b.getOctopunk())) {
			return false;
		}
		if (!controlZone.equals(b.getControlZone())) {
			return false;
		}
		if (!gameZone.equals(b.getGameZone())) {
			return false;
		}
		if (!memoryZoneX.equals(b.getXMemoryZone())) {
			return false;
		}
		if (!memoryZoneT.equals(b.getTMemoryZone())) {
			return false;
		}
		if (!stepController.equals(b.getStepController())) {
			return false;
		}
		if (!gui.equals(b.getGUI())) {
			return false;
		}
		if (time != b.getTimer()) {
			return false;
		}
		return true;
	}
	
	/**
	 * Redéfinition de la méthode hashCode.
	 */
	@Override 
	public int hashCode() {     
		return auto.hashCode() + octopunk.hashCode() + controlZone.hashCode() + gameZone.hashCode() + memoryZoneX.hashCode() + memoryZoneT.hashCode() + stepController.hashCode() + gui.hashCode();
	}
	
	/**
	 * Redéfinition de la méthode toString.
	 */
	@Override
	public String toString() {
		return "Auto button controller.\n";
	}

}
