package main.model;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
*Un objet RobotModel est un EXA définit par un String représentant son nom, un booléen indiquant si l'EXA a été terminé, où est en cours  d'exécution. Il est aussi définit par un registre X et un registre T, 
*tous deux contenant des valeurs numéraires que l'on peut modifier et utiliser dans des calculs, ainsi qu'une zone de jeu et une position dans cette parmi les troix plateformes disponibles, HOME, INBOX et 
*OUTBOX, et enfin une liste d'instructions à exécuter de sa part avec un ListIterator qui itère sur cette liste, indiquant à quel niveau d'exécution le code est. 
*
*/

public class RobotModel {
	private String name;
	private boolean alive;
	private PlatformModel position;
	private GameZoneModel GameZone;
	private List<Instruction> Instructions;
	private ListIterator<Instruction> InstructionIterator;
	private Instruction lastCalled;
	private int loopAcc;
	private Register X;
	private Register T;
	
	/**
	*Prend un String en argument et vérifie qu'il est non-null, puis crée une instance de Robot qui porte ce nom, initialise un plateforme de jeu et le positionne sur la grille home de cette dernière.
	*
	*@requires name != null
	*
	*@ensures getName() == name
	*@ensures isAlive() == true

	*@ensures getPosition() == getGameZone().getHome()
	*
	*@param name le nom du robot qu'on veut instancier.
	*
	*@throws NullPointerException si le String entré en paramètre est un pointeur null.
	*/
	public RobotModel(String name){
		if(name == null) throw new NullPointerException("Please enter a non null argument.");
		this.name = name;
		alive = true;
		GameZone = new GameZoneModel();
		position = GameZone.getHome();
		Instructions = new ArrayList<Instruction>(20);
		X = new Register("X");
		T = new Register("T");
	}
	
	/**
	*@return le nom du robot.
	*
	*@pure
	*/
	public String getName(){
		return name;
	}
	
	/**
	*@return true si le robot est en vie, false sinon.
	*
	*@pure
	*/
	public boolean isAlive(){
		return alive;
	}
	
	/**
	*@return la position sur la zone de jeu du robot.
	*
	*@pure
	*/
	public PlatformModel getPosition(){
		return position;
	}
	
	/**
	*@return la zone de jeu du robot.
	*
	*@pure
	*/
	public GameZoneModel getGameZone(){
		return GameZone;
	}
	
	/**
	*@return la valeur du registre X.
	*
	*@pure
	*/
	public int getXValue(){
		return X.getValue();
	}
	
	/**
	*@return la valeur du registre T.
	*
	*@pure
	*/
	public int getTValue(){
		return T.getValue();
	}
	
	/**
	*@return la liste d'instructions du robot.
	*
	*@pure
	*/
	public List<Instruction> getInstructions(){
		return Instructions;
	}
	
	/**
	*@return l'itérateur d'instructions.
	*
	*@pure
	*/
	public ListIterator<Instruction> getInstructionIterator(){
		return InstructionIterator;
	}
	
	/**
	*@return la dernière instruction executée par ce robot.
	*
	*@pure
	*/
	public Instruction getLastCalledInstruction(){
		return lastCalled;
	}
	
	/**
	*Méthode qui reçoit un paragraphe, le sépare en lignes, appelle le constructeur de la classe Instruction sur chaque ligne et en construit une liste d'instructions à exécuter.
	*
	*@requires codeGiven != null
	*@requires the last line be a HALT command.
	*
	*@ensures getName() == name
	*@ensures isAlive() == true
	*@ensures getInstructions().get(getInstructions().size()-1).getName().equals("HALT")
	*
	*@param codeGiven le code entré par l'utilisateur, à enregistrer comme une liste d'instructions auprès de l'EXA.
	*
	*@throws NullPointerException si le String entré en paramètre est un pointeur null.
	*@throws IllegalArgumentException si le paragraphe ne se termine pas par une instruction Halt.
	*
	*/
	public void ModifyInstructions(String codeGiven){
		if(codeGiven == null) throw new NullPointerException("Please enter a code to compile.");
		String[] instrs;
		instrs = codeGiven.split("\n");
		int i = 0;
		Instructions.clear();
		while(i < instrs.length){
			Instructions.add(new Instruction(instrs[i++]));
		}
		if(!Instructions.get(Instructions.size()-1).getName().equals("HALT")){
			throw new IllegalArgumentException("Please end your code with a HALT instruction.");
		}
		InstructionIterator = Instructions.listIterator();
		X.modifyValue(0);
		T.modifyValue(0);
		position = GameZone.getHome();
		lastCalled = null;
		alive = true;
		loopAcc = 0;
	}

	/**
	*Méthode qui exécute la prochaine instruction s'il y'en a.
	*
	*@throws UnsupportedOperationException si on crée une boucle infinie avec un argument négatif de JUMP/FJMP et qu'elle a déjà tourné 3 fois.
	*@throws NumberFormatException si on n'entre pas de valeur numéraire (entier ou registre) après pour le premier argument de COPY ou les deux premiers de ADDI/MULI/SUBI. 
	*@throws IllegalArgumentException si on n'entre pas d'entier après une commande JUMP ou FJMP.
	*@throws IllegalArgumentException si on tente d'appeler LINK avec d'autres valeurs que 800 et -1.
	*@throws IllegalArgumentException si on n'entre pas le nombre approprié d'arguments, ou que le dernier argument dans une commande de calcul n'est pas un registre.
	*@throws NoSuchElementException s'il n'y-a pas d'instruction à exécuter.
	*@throws NoSuchElementException si on tente d'avancer ou reculer quand ce n'est pas possible.
	*@throws NoSuchElementException quand on essaie de sauter en arrière plus de lignes qu'il n'y en a dans une commande JUMP ou FJMP.
	*/
	public void exec(){
		int tmp1, tmp2;
		if(!InstructionIterator.hasNext()) throw new NoSuchElementException("No instruction to execute.");
		List<String> args;
		Instruction instr = InstructionIterator.next();
		lastCalled = instr;
		args = instr.getArguments();
		switch(instr.getName()){
		case "COPY":
			if(args.size() != 2) throw new IllegalArgumentException("Please enter two arguments (Source and Destination) after a COPY command."); 
			try{
				tmp1 = convertString(args.get(0));
			}catch(NumberFormatException e){
				throw new NumberFormatException("Please enter an integer or a register (X or T)  for the first argument of a COPY command."); 
			}
			if(!Register.isRegister(args.get(1))) throw new IllegalArgumentException("Please enter a register (X or T)  for the second argument of a COPY command.");
			if(args.get(1).equals("X")) X.modifyValue(tmp1);
			else{
				T.modifyValue(tmp1);
			}
			break;
			
		case "ADDI":
			if(args.size() != 3) throw new IllegalArgumentException("Please enter three arguments after an ADDI command."); 
			try{
				tmp1 = convertString(args.get(0));
				tmp2 = convertString(args.get(1));
			}catch(NumberFormatException e){
				throw new NumberFormatException("Please enter integers or registers (X or T)  for the first two arguments of an ADDI command."); 
			}
			if(!Register.isRegister(args.get(2))) throw new IllegalArgumentException("Please enter a register (X or T)  for the third argument of an ADDI command.");
			
			if(args.get(2).equals("X")) X.modifyValue(tmp1 + tmp2);
			else{
				T.modifyValue(tmp1 + tmp2);
			}
			break;
		
		case "MULI":
			if(args.size() != 3) throw new IllegalArgumentException("Please enter three arguments after a MULI command."); 
			try{
				tmp1 = convertString(args.get(0));
				tmp2 = convertString(args.get(1));
			}catch(NumberFormatException e){
				throw new NumberFormatException("Please enter integers or registers (X or T)  for the first two arguments of a MULI command."); 
			}
			if(!Register.isRegister(args.get(2))) throw new IllegalArgumentException("Please enter a register (X or T)  for the third argument of a MULI command.");
			if(args.get(2).equals("X")) X.modifyValue(tmp1 * tmp2);
			else{
				T.modifyValue(tmp1 * tmp2);
			}
			break;
		
		case "SUBI":
			if(args.size() != 3) throw new IllegalArgumentException("Please enter three arguments after a SUBI command."); 
			try{
				tmp1 = convertString(args.get(0));
				tmp2 = convertString(args.get(1));
			}catch(NumberFormatException e){
				throw new NumberFormatException("Please enter integers or registers (X or T)  for the first two arguments of a SUBI command."); 
			}
			if(!Register.isRegister(args.get(2))) throw new IllegalArgumentException("Please enter a register (X or T)  for the third argument of a SUBI command.");
			if(args.get(2).equals("X")) X.modifyValue(tmp1 - tmp2);
			else{
				T.modifyValue(tmp1 - tmp2);
			}
			break;
		
		case "JUMP":
			if(args.size() != 1) throw new IllegalArgumentException("Please enter only one integer after a JUMP command."); 
			try{
				tmp1 = Integer.valueOf(args.get(0));
			}catch(	NumberFormatException e){
				throw new IllegalArgumentException("Please enter an integer after the JUMP command.");
			}
			while(tmp1> 0 && InstructionIterator.hasNext() ){
				InstructionIterator.next();
				tmp1--;
			}
			if(tmp1 < 0) {
				if(loopAcc > 2) throw new UnsupportedOperationException("You've created an infinite loop at: "+instr.commandLineEq());
				boolean prev = true;
				loopAcc++;
				while(tmp1<= 0 && (prev=InstructionIterator.hasPrevious())){
					InstructionIterator.previous();
					tmp1++;
				}
				if(!prev) throw new NoSuchElementException("No instruction to execute this far up.");
			}
			exec();
			break;
		
		case "FJMP":
			if(args.size() != 1) throw new IllegalArgumentException("Please enter only one integer after a FJMP command."); 
			try{
				tmp1 = Integer.valueOf(args.get(0));
			}catch(	NumberFormatException e){
				throw new IllegalArgumentException("Please enter an integer after the FJMP command.");
			}
			if(T.getValue() > 0){
				while(tmp1> 0 && InstructionIterator.hasNext() ){
					InstructionIterator.next();
					tmp1--;
				}
				if(tmp1 < 0) {
					if(loopAcc > 2) throw new UnsupportedOperationException("You've created an infinite loop at: "+instr.commandLineEq());
					boolean prev = true;
					loopAcc++;
					while(tmp1<= 0 && (prev=InstructionIterator.hasPrevious())){
						InstructionIterator.previous();
						tmp1++;
					}
					if(!prev) throw new NoSuchElementException("No instruction to execute this far up.");
				}
				exec();
			}
			break;
		
		case "LINK":
			if(args.size() != 1) throw new IllegalArgumentException("Please enter only one argument after a LINK command."); 
			switch(args.get(0)){
			case "800":
				if(! position.hasNext()) throw new NoSuchElementException("No next platform to move to.");
				position = GameZone.getNext(position);
				break;
			case "-1":
				if(! position.hasPrevious()) throw new NoSuchElementException("No previous platform to move to.");
				position = GameZone.getPrevious(position);
				break;
			default:
				throw new IllegalArgumentException("Please enter a valid port value (800 to move forward, -1 to move backwards).");
			} 
			break;
		
		case "HALT":
			if(args.size() != 0) throw new IllegalArgumentException("Please enter no argument following the HALT command.");
			this.kill();
			break;
		default:
			throw new InternalError("There should have been an exception raised while creating this instance of Instruction.");	
		}
	}
	
	/**
	*Méthode qui exécute toutes les instructions restantes.
	*
	*@ensures !this.isAlive()
	*@ensures this.getInstructions().isEmpty()
	*@ensures this.getInstructionIterator() == null
	*/
	public void execAll(){ 
		while(alive && InstructionIterator.hasNext()){
				exec();
				if(InstructionIterator == null) break;	
		}
	}
	
	/**
	*Helper method qui vérifie que la chaine de caractères entrée correspond à un entier ou un registre et renvoie la valeur de l'entier ou contenue dans le registre.
	*
	*@param a le String sensé correspondre à un entier.
	*
	*@requires a != null
	*
	*@returns si le paramètre correspond à un entier, l'entier représenté par ce string, sinon la valeur contenue dans le registre correspondant à cet identifiant.
	*
	*@throws NullPointerException si le paramètre entré est null.
	*@throws NumberFormatException si le String ne correspond ni à un entier ni à un registre.
	*/
	public int convertString(String a){
		if(a == null) throw new NullPointerException("Please enter a non null argument.");
		int tmp;
		try{
			tmp = Integer.valueOf(a);
		}catch(	NumberFormatException e){
			if(a.equals("X")){
				return X.getValue();
			}else if(a.equals("T")){
				return T.getValue();
			}
			throw new NumberFormatException("The argument entered corresponds neither to an integer nor a register (X or T)  identifier.");
		}
		return tmp;
	}
	
	/**
	*Méthode qui tue le robot.
	*
	*@ensures !this.isAlive();
	*@ensures  this.getInstructions().isEmpty();
	*@ensures this.getInstructionIterator() == null;
	*/
	public void kill(){
		InstructionIterator = null;
		alive = false;
		Instructions.clear();
	}
	
	/**
	*@return une courte description de l'EXA. S'il est mort, indique que l'EXA a été désactivé, sinon, renvoie le nom et son état d'activité .
	*
	*@ensures !this.isAlive() ==> (shortdescription().equals("This EXA has been terminated.\n")); 
	*/
	public String shortDescription(){
		if(alive == false) return "This EXA has been terminated.\n";
		return ("This is an EXA.\nIts name is: "+name+".\n");
	}
	
	/**
	*@return une longue description de l'EXA. Appelle shortdescription() puis rajoute sa position ainsi que la valeur de ses deux registres .
	*
	*@ensures !this.isAlive() ==> (shortdescription().equals("This EXA has been terminated.\n")); 
	*/
	public String longDescription(){
		if(alive == false) return "This EXA has been terminated.\n";
		String descr = ("It is in the "+position.getName()+" platform.\nRegister X:"+getXValue()+". Register T:"+getTValue()+".\n");
		return shortDescription()+descr;
	}	
	
	
	@Override 
	public int hashCode(){
		return 7*name.hashCode() + (alive? 4: 7) + 14*X.hashCode() + 12*T.hashCode() + GameZone.hashCode() + 6*position.hashCode() + (lastCalled == null ? 12 : lastCalled.hashCode()) + Instructions.hashCode();
	}

}
