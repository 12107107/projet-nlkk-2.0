package main.model;

/**
*Une classe qui représente une grille où l'EXA peut se déplacer. Une platform est définie par son nom, et si elle a une grille qui la suit ou la précède.
*
*@invariant hasNext() || hasPrev()
*@invariant getName() != null
*/
public class PlatformModel implements Cloneable {
	private String name;
	private boolean hasNext;
	private boolean hasPrev;
	
	/**
	*Prend un String en argument et vérifie qu'il est non-null et qu'il correspond à un nom de grille valide, puis crée une instance de platform qui porte ce nom et a les 			
	*caractéristiques correspondates.
	*
	*@requires name != null && (name.equals("HOME") || name.equals("INBOX") || name.equals("OUTBOX"))
	*
	*@ensures getName() == name
	*@ensures ((getName() == "HOME" && hasNext() && !hasPrev())|| (getName() == "INBOX" && hasNext() && hasPrev())||( getName() == "OUTBOX" && !hasNext() && hasPrev()))
	*
	*@param name le nom de la grille qu'on veut instancier.
	*
	*@throws IllegalArgumentException si le nom entré en paramètre n'est ni HOME, ni INBOX, ni OUTBOX. 
	*@throws NullPointerException si le String entré en paramètre est un pointeur null.
	*/
	public PlatformModel(String name){
		if(name == null) throw new NullPointerException("Please enter a non null argument.");
		switch(name){
		case "HOME":
			this.name = name;
			hasPrev = false;
			hasNext = true;
			break;
		case "INBOX":
			this.name = name;
			hasPrev = true;
			hasNext = true;
			break;
		case "OUTBOX":
			this.name = name;
			hasPrev = true;
			hasNext = false;
			break;
		default:
			throw new IllegalArgumentException("Please enter either HOME, INBOX or OUTBOX to create a platform.");
		}
	}
	
	/**
	*Renvoie le nom de la grille.
	*
	*@return le nom de la grille.
	*
	*@pure
	*/
	
	public String getName(){
		return name;
	}
	
	/**
	*Indique s'il y'a une grille qui suit.
	*
	*@return true si on peut avancer vers une autre grille, false sinon.
	*
	*@pure
	*/
	public boolean hasNext(){
		return hasNext;
	}
	
	/**
	*Indique s'il y'a une grille qui précède.
	*
	*@return true si on peut reculer vers une autre grille, false sinon.
	*
	*@pure
	*/
	public boolean hasPrevious(){
		return hasPrev;
	}
	
	@Override
	public String toString(){
		return "This is the "+name+" platform.";
	}	
	
	@Override 
	public int hashCode(){
		return 10*name.hashCode() + (hasNext? 28: 0) + (hasPrev? 37: 0) ;
	}
	
	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof PlatformModel)) return false;
		PlatformModel platform = (PlatformModel) obj;
		return name.equals(platform.name) && hasNext == platform.hasNext && hasPrev == platform.hasPrev;
	}
	

	public PlatformModel clone(){
		PlatformModel clone;
		try{
			clone = (PlatformModel) super.clone();
		}catch(CloneNotSupportedException e){
			throw new InternalError("clone should work.");
		}
		clone.name = this.name; 
		clone.hasPrev = this.hasPrev;
		clone.hasNext = this.hasNext;
		return clone;
	}
	
}
