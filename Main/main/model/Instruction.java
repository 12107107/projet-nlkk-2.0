package main.model;

import java.util.ArrayList;
import java.util.List;
import java.lang.String;

/**
*Instruction est une classe définie par un nom d'instruction de type String, et une liste d'arguments de type String. 
*
*@invariant INSTRUCTNAME.isValid(this.getName());
*/
public class Instruction implements Cloneable{
	public String name;
	public List<String> Arguments;
	
	/**
	*Constructeur qui prend en paramètre un String correspondant à la ligne de code à interpréter en Instruction.
	*
	*@param line la ligne que l'on veut parser en un nom d'instruction et des arguments.  
	*
	*@requires line != null
	*
	*@ensures this.getName() != null;
	*@ensures this.commandLineEq().equals(line.trim());
	*
	*@throws NullPointerException si le paramètre entré est null.
	*@throws IllegalArgumentException si le premier mot du String entré ne figure pas dans la liste des noms d'instruction admis.
	*/
	public Instruction(String line){
		if(line == null){
			throw new NullPointerException("Non null command line required.");
		}
		line.trim();
		int i = 0, lineLength = line.length(), j;
		char[] charName = new char[100];
		char[] arg = new char[100];
		String tmp ;
		for(i = 0; i < lineLength && line.charAt(i) != ' '; i++) charName[i] = line.charAt(i);
		tmp = new String(charName, 0, i);
		if(!(INSTRUCTNAME.isValid(tmp))){
			throw new IllegalArgumentException("Please enter a valid instruction name.");
		}
		name = tmp;
		Arguments = new ArrayList<String>(3);
		i = tmp.length()+1;
		while(i < lineLength){
			arg = new char[100];
			j = 0;
			while(i < lineLength && line.charAt(i) != ' '){
				arg[j++] = line.charAt(i);
				i++;
			}
			if(arg.length == 0) break;
			Arguments.add(new String(arg, 0, j));	
			i++;	
		}
	}
	
	/**
	*@returns le nom d'instruction. 
	*
	*@pure
	*/
	public String getName(){
		return name;
	}
	
	/**
	*@returns la liste des arguments de l'instruction. 
	*
	*@pure
	*/
	public List<String> getArguments(){
		return Arguments;
	}
	
	/**
	*Méthode qui renvoie le un String équivalent à celui entré par l'utilisateur pour cette commande. 
	*
	*@returns l'équivalent de cette instruction en chaine de caractères. 
	*/
	public String commandLineEq(){
		String tmp = "";
		for(String s : Arguments){
			tmp = tmp.concat(s+" ");
		}
		return name.concat(" "+tmp.trim());
	}
	
	@Override
	public String toString(){
		String description = "This is an instruction that corresponds to the following command line: "+name+" ";
		for(String tmp: Arguments) description = description.concat(tmp+" ");
		return description+".";
	}	
	
	@Override 
	public int hashCode(){
		int hash = 5*name.hashCode();
		for(String tmp: Arguments) hash += tmp.hashCode();
		return hash;
	}
	
	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof Instruction)) return false;
		Instruction instr = (Instruction) obj;
		return name.equals(instr.name) && Arguments.equals(instr.Arguments);
	}
	
	@Override
	public Instruction clone(){
		Instruction clone;
		try{
			clone = (Instruction) super.clone();
		}catch(CloneNotSupportedException e){
			throw new InternalError("clone should work.");
		}
		clone.name = this.name; 
		clone.Arguments = new ArrayList<String>(this.Arguments);
		return clone;
	}
	
}

