package main.model;

/**
*Register est une classe définie par un identifiant de type String, pouvant prendre la valeur "X" ou "T", et un contenu de type Int, que l'on peut modifier et utiliser dans des calculs de type addition, 
*soustraction ou multiplication. 
*
*@invariant this.getId().equals("X") || this.getId().equals("T")
*/
public class Register implements Cloneable{
	private String identifier;
	private int content;
	
	/**
	*Constructeur qui prend en paramètre un String correspondant à l'identifiant de l'instance de Register qui sera créée.
	*
	*@param id un String correspondant à l'identifiant du registre que l'on veut instancier.
	*
	*@requires id != null && (id.equals("X") || id.equals("T"))
	*
	*@ensures this.getId() != null
	*@ensures this.getValue() == 0
	*@ensures this.getId() == id
	*
	*@throws NullPointerException si le paramètre entré est null.
	*@throws IllegalArgumentException si le String entré en paramètre n'est ni "x", ni "T".
	*/
	public Register(String id){
		if(id == null) throw new NullPointerException("Please enter a non null identifier.");
		if(!Register.isRegister(id)) throw new IllegalArgumentException("In this version of the game, only the X and T registers are to be manipulated.");
		identifier = id;
		content = 0;
	}
	
	/**
	*Méthode qui prend un entier en paramètre et modifie la valeur du registre.
	*
	*@param val l'entier qui remplacera la valeur de notre registre.
	*
	*@ensures val == this.getValue();
	*/
	public void modifyValue(int val){
		content = val;
	}
	
	/**
	*@returns l'identifiant du registre. 
	*
	*@pure
	*/
	public String getId(){
		return identifier;
	}
	
	/**
	*@returns la valeur contenue dans le registre. 
	*
	*@pure
	*/
	public int getValue(){
		return content;
	}
	
	/**Méthode qui renvoie true si le string entré en paramètre correspond bien à un identifiant de registre, et false sinon. 
	*
	*@param regname le String qu'on déterminera comme étant un identifiant de registre ou pas.
	*
	*@requires regname != null
	*
	*@ensures (regname.equals("X") || regname.equals("T")) <==> result
	*
	*@throws NullPointerArgument si regname est null.
	*/
	public static boolean isRegister(String regname){
		if(regname == null) throw new NullPointerException("Please enter a non null argument.");
		if(regname.equals("T") || regname.equals("X")) return true;
		return false;
	}
	
	@Override
	public String toString(){
		return ("This is the register called: "+identifier+", of value:"+content+".\n");
	}	
	
	@Override 
	public int hashCode(){
		return (identifier.hashCode() + 45*content);
	}
	
	@Override
	protected Register clone(){
		Register clone;
		try{
			clone = (Register) super.clone();
		}catch(CloneNotSupportedException e){
			throw new InternalError("clone should work.");
		}
		clone.identifier = this.identifier; 
		clone.content = this.content;
		return clone;
	}
	
	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof Register)) return false;
		Register reg = (Register) obj;
		return this.identifier.equals(reg.identifier) && this.content == reg.content;
	}
}
