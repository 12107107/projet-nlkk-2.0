package main.model;

/**
*Classe sans attributs dont la seule fonction est de déterminer si un string correspond à un nom d'instruction admis.
*/
public class INSTRUCTNAME{
	  
	  /**
	  *Méthode statique prenant en paramètre un string et renvoyant true seulement s'il fait partie des noms d'instruction admis.
	  *
	  *@param name un String qu'on teste pour savoir si c'est un nom d'instruction autorisé.
	  *
	  *@ensures (result == true) <==> (name.equals("COPY") || name.equals("ADDI") || name.equals("MULI") || name.equals("SUBI") || name.equals("JUMP") || name.equals("FJMP") || name.equals("LINK") || name.equals("HALT"))
	  *
	  *@returns true si le paramètre est un nom d'instruction, false sinon.
	  */
	  public static boolean isValid(String name){
	  	switch(name){
	  	case "COPY":
	  	case "ADDI":
	  	case "MULI":
	  	case "SUBI":
	  	case "JUMP":
	  	case "FJMP":	
	  	case "LINK":
	  	case "HALT":
	  		return true;
	  	default: 
	  		return false;
	  	}
	  }
}
