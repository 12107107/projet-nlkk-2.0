package main.model;

import java.util.NoSuchElementException;

/**
*Classe définie par trois plateformes home, inbox, outbox, qui serviront de zone de jeu pour l'utilisateur, où l'EXA pourra se déplacer. 
*
*@invariant getHome() != null && getInBox() != null && getOutBox() != null 
*/
public class GameZoneModel implements Cloneable{
	private PlatformModel home;
	private PlatformModel inbox;
	private PlatformModel outbox;
	
	/**
	*Constructeur sans arguments qui crée une nouvelle instance de GameZoneModel avec une plateforme home, inbox et outbox.
	*/
	public GameZoneModel(){
		home = new PlatformModel("HOME");
		inbox = new PlatformModel("INBOX");
		outbox = new PlatformModel("OUTBOX");
	}
	
	/**
	*@return le grille home de cette plateforme.
	*
	*@pure
	*/
	public PlatformModel getHome(){
		return home;
	}
	
	/**
	*@return la grille inbox de cette plateforme.
	*
	*@pure
	*/
	public PlatformModel getInBox(){
		return inbox;
	}
	
	/**
	*@return la grille outbox de cette plateforme.
	*
	*@pure
	*/
	public PlatformModel getOutBox(){
		return outbox;
	}
	
	/**
	*Méthode qui renvoie la grille précédente si elle existe.
	*
	*@param position la position dont on veut avoir la plateforme précédente.
	*
	*@requires position != null
	*@requires position == getInBox() || position == getOutBox()
	*
	*@ensures result == getInBox() ==> position == getOutBox() 
	*@ensures result == getHome() ==> position == getInBox() 
	*
	*@returns la plateforme précédant celle entrée en paramètre.
	*
	*@throws NullPointerException si le paramètre entré est null.
	*@throws NoSuchElementException si la plateforme entrée en paramètre n'a pas de plateforme précédente (correspond à une plateforme HOME).
	*@throws IllegalArgumentException si la plateforme entrée n'est pas l'une des plateformes de cette zone de jeu.
	*/
	public PlatformModel getPrevious(PlatformModel position){
		if(position == null) throw new NullPointerException("Please enter a non null argument.");
		if(!position.hasPrevious()){
			throw new NoSuchElementException("Please only enter a grid that has another grid following it.");
		}
		else if(position == inbox){
			return home;
		}
		else if(position == outbox){
			return inbox;
		}
		else{
			throw new IllegalArgumentException("The platform entered is not on this gamezone.");
		}
	
	}
	
	/**
	*Méthode qui renvoie la grille suivante si elle existe.
	*
	*@param position la position dont on veut avoir la plateforme suivante.
	*
	*@requires position != null
	*@requires position == getOutBox() || position == getHome()
	*
	*@ensures result == getOutBox() ==> position == getInBox() 
	*@ensures result == getInBox() ==> position == getHome() 
	*
	*@returns la plateforme suivant celle entrée en paramètre.
	*
	*@throws NullPointerException si le paramètre entré est null.
	*@throws NoSuchElementException si la plateforme entrée en paramètre n'a pas de plateforme suivante (correspond à une plateforme OUTBOX).
	*@throws IllegalArgumentException si la plateforme entrée n'est pas l'une des plateformes de cette zone de jeu.
	*/
	public PlatformModel getNext(PlatformModel position){
		if(position == null) throw new NullPointerException("Please enter a non null argument.");
		if(!position.hasNext()) throw new NoSuchElementException("Please only enter a grid that has another grid following it.");
		if(position == home){
			return inbox;
		}
		else if(position == inbox){
			return outbox;
		}
		else{
			throw new IllegalArgumentException("The grid entered is not on this platform.");
		}
	
	}
	
	@Override
	public String toString(){
		return "This is a game zone.";
	}	
	
	@Override 
	public int hashCode(){
		return home.hashCode() + 40*inbox.hashCode() + outbox.hashCode() ;
	}
	
	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof GameZoneModel)) return false;
		GameZoneModel platform = (GameZoneModel) obj;
		return  home.equals(platform.home) && inbox.equals(platform.inbox) && outbox.equals(platform.outbox);
	}
	
	@Override
	public GameZoneModel clone(){
		GameZoneModel clone;
		try{
			clone = (GameZoneModel) super.clone();
		}catch(CloneNotSupportedException e){
			throw new InternalError("clone should work.");
		}
		clone.home = home.clone(); 
		clone.inbox = inbox.clone();
		clone.outbox = outbox.clone();
		return clone;
	}
}
