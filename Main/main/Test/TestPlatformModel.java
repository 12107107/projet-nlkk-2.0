package main.model ;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

public class TestPlatformModel {
    @Test
    public void testPlatformModelCreation() {
        // Crée une platforme HOME et vérifie qu'il  y a pas de platfome avant la platforme "HOME" et qu'il y a des platforme" INBOX " apres
        PlatformModel homePlatformModel = new PlatformModel("HOME");
        assertEquals("HOME", homePlatformModel.getName());
        assertTrue(homePlatformModel.hasNext());
        assertFalse(homePlatformModel.hasPrevious());

        // Crée une platforme INBOX et vérifie qu'il  y a   la platforme " HOME " avant et la platforme " OUTBOX " apres
        PlatformModel inboxPlatformModel = new PlatformModel("INBOX");
        assertEquals("INBOX", inboxPlatformModel.getName());
        assertTrue(inboxPlatformModel.hasNext());
        assertTrue(inboxPlatformModel.hasPrevious());

        // Crée une platforme OUTBOX et vérifie qu'il  y a pas de platfome apres la platforme  et qu'il y a des platforme " INBOX "avant
        PlatformModel outboxPlatformModel = new PlatformModel("OUTBOX");
        assertEquals("OUTBOX", outboxPlatformModel.getName());
        assertFalse(outboxPlatformModel.hasNext());
        assertTrue(outboxPlatformModel.hasPrevious());
    }
    //    //  test si la plateforme  a ete crée avec un autre argument que "home" " inbox" " outbox"
    @Test
    public void testInvalidPlatformModelCreation() {
    
        assertThrows(IllegalArgumentException.class, () ->  new PlatformModel("Invalid"));
    }

    // test si la platforme a été crée sans nom
    @Test
    public void testPlatformModelNullCreation(){
        assertThrows(NullPointerException.class, () ->  new PlatformModel(null));
        
    }



    // Crée deux platforme qui ont le meme nom et vérifie leur égalité
    @Test
    public void testPlatformModelEquality() {

        PlatformModel PlatformModel1 = new PlatformModel("HOME");
        PlatformModel PlatformModel2 = new PlatformModel("HOME");
        PlatformModel PlatformModel3 = new PlatformModel("OUTBOX");
        PlatformModel PlatformModel4 = new PlatformModel("OUTBOX");
        PlatformModel PlatformModel5 = new PlatformModel("INBOX");
        PlatformModel PlatformModel6 = new PlatformModel("INBOX");
        assertEquals(PlatformModel1, PlatformModel2);
        assertEquals(PlatformModel3, PlatformModel4);
        assertEquals(PlatformModel5, PlatformModel6);

        // Vérifie que deux platformes de nom different sont pas les mêmes
        assertNotEquals(PlatformModel1, PlatformModel5);
        assertNotEquals(PlatformModel1, PlatformModel3);
        assertNotEquals(PlatformModel3, PlatformModel5);
        assertNotEquals(PlatformModel3, PlatformModel1);
        assertNotEquals(PlatformModel5, PlatformModel1);
        assertNotEquals(PlatformModel5, PlatformModel3);
        // Vérifie que la platforme n'est pas égale à un objet non lié à PlatformModel
        assertNotEquals(PlatformModel1, new Object());
        assertNotEquals(PlatformModel3, new Object());
        assertNotEquals(PlatformModel5, new Object());
    }


    @Test
    public void testPlatformModelClone() {

        PlatformModel PlatformModel1 = new PlatformModel("HOME");
        PlatformModel PlatformModel2 = new PlatformModel("INBOX");
        PlatformModel PlatformModel3 = new PlatformModel("OUTBOX");
        PlatformModel clonedPlatformModel1 = PlatformModel1.clone();
        PlatformModel clonedPlatformModel2 = PlatformModel2.clone();
        PlatformModel clonedPlatformModel3 = PlatformModel3.clone();

        // Vérifie que les deux platformes sont égales mais différentes en termes d'objets
        assertEquals(PlatformModel1, clonedPlatformModel1);
        assertNotSame(PlatformModel1, clonedPlatformModel1);
        assertEquals(PlatformModel2, clonedPlatformModel2);
        assertNotSame(PlatformModel2, clonedPlatformModel2);
        assertEquals(PlatformModel3, clonedPlatformModel3);
        assertNotSame(PlatformModel3, clonedPlatformModel3);
    }
}
