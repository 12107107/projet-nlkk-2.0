package main.model;

import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals; 
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

class TestRobotModel {

    // Test d'initialisation du robot
    @Test
    void testRobotModelInitialization() {
        RobotModel robot = new RobotModel("TestRobot");

        assertEquals("TestRobot", robot.getName());
        assertTrue(robot.isAlive());
        assertFalse(robot.isActive());
        assertNotNull(robot.getPosition());
        assertNotNull(robot.getGameZone());
        assertEquals(robot.getPosition(), robot.getGameZone().getHome());
        assertEquals(0, robot.getXValue());
        assertEquals(0, robot.getTValue());
        assertNotNull(robot.getInstructions());
        assertNotNull(robot.getInstructionIterator());
        assertNull(robot.getLastCalledInstruction());
    }

    // Test de la méthode de modification des instructions
    @Test
    void testModifyInstructions() {
        RobotModel robot = new RobotModel("TestRobot");
        String code = "COPY 5 X\nADDI 3 2 T\nHALT";

        robot.ModifyInstructions(code);

        List<Instruction> instructions = robot.getInstructions();
        assertEquals(3, instructions.size());
        assertEquals("COPY", instructions.get(0).getName());
        assertEquals("ADDI", instructions.get(1).getName());
        assertEquals("HALT", instructions.get(2).getName());
    }

    // Test de l'exécution de toutes les instructions
    @Test
    void testExecAll() {
        RobotModel robot = new RobotModel("TestRobot");
        String code = "COPY 5 X\nADDI 3 2 T\nHALT";

        robot.ModifyInstructions(code);
        robot.execAll();

        assertEquals(5, robot.getXValue());
        assertEquals(5, robot.getTValue());
    }

    // Test de la méthode de suppression du robot
    @Test
    void testKill() {
        RobotModel robot = new RobotModel("TestRobot");
        robot.kill();

        assertFalse(robot.isAlive());
        assertNull(robot.getInstructionIterator());
        assertFalse(robot.isActive());
        assertTrue(robot.getInstructions().isEmpty());
    }

    // Test de la description courte du robot
    @Test
    void testShortDescription() {
        RobotModel robot = new RobotModel("TestRobot");
        String description = robot.shortDescription();

        assertTrue(description.contains("TestRobot"));
        assertTrue(description.contains("not active"));
    }

    // Test de la description longue du robot
    @Test
    void testLongDescription() {
        RobotModel robot = new RobotModel("TestRobot");
        String description = robot.longDescription();

        assertTrue(description.contains("TestRobot"));
        assertTrue(description.contains("not active"));
        assertTrue(description.contains("Register X:0"));
        assertTrue(description.contains("Register T:0"));
        assertTrue(description.contains("grid"));
    }

    // Test de l'exécution d'une instruction COPY
    @Test
    void testExecCOPY() {
        RobotModel robot = new RobotModel("TestRobot");
        String code = "COPY 5 X\nHALT";

        robot.ModifyInstructions(code);
        robot.exec();
        assertEquals(5, robot.getXValue());
    }

    // Test de l'exécution d'une instruction ADDI
    @Test
    void testExecADDI() {
        RobotModel robot = new RobotModel("TestRobot");
        String code = "ADDI 3 2 T\nHALT";

        robot.ModifyInstructions(code);
        robot.exec();
        assertEquals(5, robot.getTValue());
    }

    // Test de l'exécution d'une instruction MULI
    @Test
    void testExecMULI() {
        RobotModel robot = new RobotModel("TestRobot");
        String code = "MULI 3 2 T\nHALT";

        robot.ModifyInstructions(code);
        robot.exec();
        assertEquals(6, robot.getTValue());
    }

    // Test de l'exécution d'une instruction SUBI
    @Test
    void testExecSUBI() {
        RobotModel robot = new RobotModel("TestRobot");
        String code = "SUBI 5 3 T\nHALT";

        robot.ModifyInstructions(code);
        robot.exec();
        assertEquals(2, robot.getTValue());
    }

    // Test de l'exécution d'une instruction JUMP
    @Test
    void testExecJUMP() {
        RobotModel robot = new RobotModel("TestRobot");
        String code = "JUMP 2\nADDI 3 2 T\nHALT";

        robot.ModifyInstructions(code);
        robot.exec();
        assertEquals(0, robot.getTValue());
    }

    // Test de l'exécution d'une instruction FJUMP
    @Test
    void testExecFJUMP() {
        RobotModel robot = new RobotModel("TestRobot");
        String code = "FJUMP 2\nADDI 3 2 T\nHALT";

        robot.ModifyInstructions(code);
        robot.exec();
        assertEquals(0, robot.getTValue());
    }

    // Test de l'exécution d'une instruction LINK
    @Test
    void testExecLINK() {
        RobotModel robot = new RobotModel("TestRobot");
        String code = "LINK 800\nHALT";

        robot.ModifyInstructions(code);
        robot.exec();
        assertEquals("North", robot.getPosition().getName());
    }
}