package main.model;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;


import org.junit.Test;

public class TestINSTRUCTNAME{
// test les nom des instructions bien ecrit qui sont listé 
    @Test
    public void testValidName() {
        assertTrue(INSTRUCTNAME.isValid("COPY"));
        assertTrue(INSTRUCTNAME.isValid("ADDI"));
        assertTrue(INSTRUCTNAME.isValid("MULI"));
        assertTrue(INSTRUCTNAME.isValid("SUBI"));
        assertTrue(INSTRUCTNAME.isValid("JUMP"));
        assertTrue(INSTRUCTNAME.isValid("FJMP"));
        assertTrue(INSTRUCTNAME.isValid("LINK"));
        assertTrue(INSTRUCTNAME.isValid("HALT"));
    }
// test s'il s'agit d'un autre argument
    @Test
    public void testInvalidName() {
        assertFalse(INSTRUCTNAME.isValid("INVALID"));

    }
}
