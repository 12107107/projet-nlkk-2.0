package main.model;

import org.junit.Test;


import java.util.NoSuchElementException;

import static org.junit.Assert.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals; 
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

public class TestGameZoneModel {
//test si les 3 plateformes sont bien crée
    @Test
    public void testPlatformCreation() {
        GameZoneModel platform = new GameZoneModel(); 
        assertEquals("HOME", platform.getHome().getName());
        assertEquals("INBOX", platform.getInBox().getName()); 
        assertEquals("OUTBOX", platform.getOutBox().getName()); 
    }
// test si il y a une plateforme avant   pour chaque platforme ou l'on est  ( sauf pour home ) 
    @Test
    public void PreviousPlatform() {
        GameZoneModel platform = new GameZoneModel(); 
        assertEquals("HOME", platform.getPrevious(platform.getInBox()).getName());
        assertEquals("INBOX", platform.getPrevious(platform.getOutBox()).getName());
    }
// test le cas ou il n'y pas de plateforme avant home 
    @Test
    public void NotPreviousPlatform() {
        GameZoneModel platform = new GameZoneModel(); 

        assertThrows(NoSuchElementException.class, () -> platform.getPrevious(platform.getHome())); 
    }
//// test si il y a une plateforme apres  pour chaque platforme ou l'on est  ( sauf pour outbox ) 
    @Test
    public void NextPlatform() {
        GameZoneModel platform = new GameZoneModel();
        assertEquals("INBOX", platform.getNext(platform.getHome()).getName());
        assertEquals("OUTBOX", platform.getNext(platform.getInBox()).getName());
    }
//// test le cas ou il n'y pas de plateforme apres  outbox
    @Test
    public void NotNextPlatform() {
        GameZoneModel platform = new GameZoneModel(); 

        assertThrows(NoSuchElementException.class, () -> platform.getNext(platform.getOutBox())); 
    }
// Crée deux GameZoneModel qui ont le meme nom et vérifie leur égalite
    @Test
    public void testPlatformEquality() {
        GameZoneModel platform1 = new GameZoneModel();
        GameZoneModel platform2 = new GameZoneModel();


        assertEquals(platform1, platform2);


        assertNotEquals(platform1, new Object());

    }

    @Test
    public void testPlatformClone() {
        GameZoneModel platform1 = new GameZoneModel();

        GameZoneModel clonedPlatform1 = platform1.clone();
  // Vérifie que les deux GameZoneModel sont égales mais différentes en termes d'objets
        assertEquals(platform1, clonedPlatform1);
        assertNotSame(platform1, clonedPlatform1);

    }
}
