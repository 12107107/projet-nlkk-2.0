package main.model;

import org.junit.Test;


import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;


public class TestRegister {

@Test
    public void testRegisterCreation() {
        Register register1 = new Register("X");
        Register register2 = new Register("T");

        // Vérifie que le registre est correctement initialisé
        assertEquals("X", register1.getId());
        assertEquals(0, register1.getValue());
        assertEquals("T", register2.getId());
        assertEquals(0, register2.getValue());
    }
    @Test
    public void testIsRegister(){
        assertTrue(Register.isRegister("X"));
        assertTrue(Register.isRegister("T"));
        assertFalse(Register.isRegister("INVALID"));


    }
@Test
    public void testModifyValue() {
        Register register1 = new Register("T");
        Register register2 = new Register("X");

        register1.modifyValue(28);
        assertEquals(28, register1.getValue());
        register2.modifyValue(5);
        assertEquals(5, register2.getValue());
    }
    @Test
    public void testIsRegisterInvalid() {
        assertThrows(NullPointerException.class, () -> Register.isRegister(null));
    }

@Test
    public void testRegisterEquality() {
        Register register1 = new Register("X");
        Register register2 = new Register("X");
        Register register3 = new Register("T");
        Register register4 = new Register("T");

        assertEquals(register3, register4);
        // comment faire en sorte de modifier un des deux registre et dire que ce ne sont pas les meme 
        register3.modifyValue(9);
        assertNotEquals(register3, register4);
        assertNotEquals(register3, new Object());

        assertEquals(register1, register2);
        // comment faire en sorte de modifier un des deux registre et dire que ce ne sont pas les meme 
        register1.modifyValue(9);
        assertNotEquals(register1, register2);
        assertNotEquals(register1, new Object());
    }
    @Test
    public void testRegisterClone() {
        Register Register1 = new Register("T");
        Register Register2 = new Register("X");
        Register clonedRegister1 = Register1.clone();
        Register clonedRegister2 = Register2.clone();

        // Vérifie que les deux registres sont égaux mais différents en termes d'objets
        assertEquals(Register1, clonedRegister1);
        assertNotSame(Register1, clonedRegister1);
        assertEquals(Register2, clonedRegister2);
        assertNotSame(Register2, clonedRegister2);
    }

    }
