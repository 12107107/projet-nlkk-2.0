package main.model ;
import org.junit.Test;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

class TestInstruction {

    // Test de création d'une instruction valide
    @Test
    void testValidInstructionCreation() {
        String line = "COPY 5 X";
        Instruction instruction = new Instruction(line);

        assertEquals("COPY", instruction.getName());
        List<String> arguments = instruction.getArguments();
        assertEquals(2, arguments.size());
        assertEquals("5", arguments.get(0));
        assertEquals("X", arguments.get(1));
    }

    // Test de création d'une instruction invalide (nom invalide)
    @Test
    void testInvalidInstructionCreation() {
        String line = "INVALID_INSTRUCTION 5 X";
        assertThrows(IllegalArgumentException.class, () -> new Instruction(line));
    }

    // Test de création d'une instruction invalide (ligne null)
    @Test
    void testNullInstructionCreation() {
        assertThrows(NullPointerException.class, () -> new Instruction(null));
    }

    // Test de la méthode toString
    @Test
    void testToString() {
        String line = "COPY 5 X";
        Instruction instruction = new Instruction(line);

        assertEquals("This is an instruction that corresponds to the following command line: COPY 5 X .", instruction.toString());
    }

    // Test de la méthode clone
    @Test
    void testClone() {
        String line = "COPY 5 X";
        Instruction instruction = new Instruction(line);
        Instruction clone = instruction.clone();

        assertEquals(instruction.getName(), clone.getName());
        assertEquals(instruction.getArguments(), clone.getArguments());
    }
}
