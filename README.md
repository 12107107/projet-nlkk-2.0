# Octopunks

## Description
Octopunks est un jeu où on écrit du code manipulant un robot doté de registres X et T et capable de faire divers calculs et de se déplacer sur une zone de jeu entre des plateformes, avant de disparaitre 
sans laisser de trace. 

***

## Installation
Assurez vous d'avoir installé le package java avant de compiler ce jeu.

Dans le cas où vous ne l'avez pas, tapez "sudo apt update" sur votre terminal, puis "sudo apt upgrade" si vos packages ne sont pas à jour; sinon tapez directement "sudo apt install java". Vous devriez pouvoir utiliser javac pour compiler.

## Utilisation
Pour pouvoir utiliser ce jeu, faites du répertoire projet-nlkk-2.0/Main votre répertoire courant.

Pour lancer le jeu tapez "java main.Octopunks" sur votre shell.

Pour pouvoir compiler toutes les classes du jeu, tapez "javac main/Octopunks.java main/model/\*.java main/view/\*.java main/controller/*.java".

## Description Détaillée
Après avoir lancé le jeu, vous pouvez entrer du code dans la fenêtre réservée à cet effet. 

Après avoir fini d'écrire votre code assembleur, appuyez sur le bouton Step pour exécuter une commande à la fois, et le bouton Auto pour exécuter toutes les instructions à la fois. 

Après avoir lancé l'exécution des instructions avec le bouton Step, vous ne pourrez plus les modifier à moins que vous intérrompiez cette dernière avec le bouton Stop. Vous pourrez ainsi de nouveau écrire dans la fenêtre dédiée à la réception de code assembleur. Le bouton Auto ne peut s'interrompre avec le bouton Stop; il exécute toutes les commandes d'un trait et ne s'arrête que lorsqu'il atteint un HALT.

Voici les règles du jeu :

	Vous ne pouvez entrer d'instructions qui n'aient pas été définies dans l'index qui suit.

	Vous devez vous conformer strictement à la syntaxe énoncée dans l'index ci-dessous.

	Vous ne pouvez pas avancer depuis la troisième plateforme, ou reculer depuis la première.

	Vous devez finir votre code par une instruction HALT, pour permettre au robot de disparaitre sans laisser de traces après avoir terminé.

## Index Des Instructions
ADDI : Prend trois arguments; deux premiers arguments représentant soit des entiers ou les registres de l'Octopunks, et un troisième représentant le registre de l'Octopunks qui reçoit l'addition des deux valeurs précedemment entrées.

MULI : Prend trois arguments; deux premiers arguments représentant soit des entiers ou les registres de l'Octopunks, et un troisième représentant le registre de l'Octopunks qui reçoit la multiplication des deux valeurs précedemment entrées.

SUBI : Prend trois arguments; deux premiers arguments représentant soit des entiers ou les registres de l'Octopunks, et un troisième représentant le registre de l'Octopunks qui reçoit la soustraction des deux valeurs précedemment entrées.

COPY : Prend deux arguments; le premier représentant soit un entier ou un des registres de l'Octopunks, et un deuxième représentant le registre de l'Octopunks où l'on copie la première valeur.

LINK : Prend un unique argument; 800 pour avancer l'Octopunks d'une plateforme et -1 pour le reculer d'une plateforme.

JUMP : Prend un unique argument; le nombre d'instructions à sauter. Si on atteint HALT avant d'avoir atteint le nombre d'instructions à sauter, l'Octopunks se contente de terminer. Ps:Cette valeur peut être négative, ce qui aurait pour effet de remonter dans la liste d'instructions du nombre indiqué d'instructions.

FJMP : Même chose que JUMP, mais vérifie que le registre T contient une valeur positive. Si oui, cette commande a le même comportement que JUMP suivie de son argument, sinon elle ne fait rien.

HALT : Ne prend aucun argument. Termine l'Octopunks et le fait disparaitre de la zone de jeu. 

## Exemples d'utilisation des commandes
	ADDI 7 X T
	MULI X 4 X
	SUBI 8 5 T
	COPY 14 X
	COPY X T
	JUMP 14
	FJMP -5
	LINK 800
	LINK -1

Ps: Dans ces exemples, on pourrait remplacer ADDI, MULI, SUBI entre elles et ce serait toujours syntaxiquement correct.

## Contact
Pour toute question, veuillez nous contacter sur l'email du coordinateur : kevin.madivanane@edu.univ-paris13.fr .

## Auteurs
*Kevin MADIVANANE* Coordinateur de projet.

*Nawel HEBRI* Interface graphique.

*Lina MEHIDI* Code métier & Entrée-Sortie textuelle.

*Karim MAHTOUT* L'édition d'une partie des classes de test, et des exemples de code assembleur.

## Statut du projet.
Fini.
